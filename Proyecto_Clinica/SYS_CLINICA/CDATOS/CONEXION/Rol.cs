﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Rol
    {
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;

        #region Constructor
        #endregion

        #region Insertar Paciente
        public void MInsertarRol(string descripcion,string nombre)
        {
            consulta = new SqlCommand("Sp_insertar_rol", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Actualizar Paciente
        public void MModificarRol(string nombre,string descripcion,int Codigo)
        {
            consulta = new SqlCommand("Sp_actualizar_rol", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@Codigo", Codigo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Eliminar Paciente
        public void MEliminarRol(int Codigo)
        {
            consulta = new SqlCommand("Sp_eliminar_rol", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Codigo", Codigo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }
        #endregion

        #region ConsultarRoles
        public DataSet MConsultarTodosRoles()
        {
            consulta = new SqlCommand("Sp_trae_todos_roles", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public DataSet MConsultarTodosRolesCombo()
        {
            consulta = new SqlCommand("Sp_trae_roles_combo", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        #endregion

    }
}
