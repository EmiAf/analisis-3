﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Incapacidades
    {
        #region Variables
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;
        #endregion

        #region Constructor
        public Incapacidades()
        {

        }
        #endregion
        
        #region Insertar Incapacidad
        public void MInsertarIncapacidad(int tipo_incapacidad, string Cedula_Paciente, DateTime Fec_Emision, DateTime Fec_Inicio, DateTime Fec_Final, string Descripcion)
        {
            consulta = new SqlCommand("Sp_insertar_incapacidad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Incapacidad", tipo_incapacidad));
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", Cedula_Paciente));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Emision", Fec_Emision));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Inicio", Fec_Inicio));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Final", Fec_Final));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", Descripcion));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Actualizar Incapacidad
        public void MModificarIncapacidad(int IdIncapacidad, int TipoIncapacidad, string cedula, DateTime FecEmision, DateTime FecInicio, DateTime FecFinal, string descripcion)
        {
            consulta = new SqlCommand("Sp_actualizar_incapacidad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Incapacidad", IdIncapacidad));
            consulta.Parameters.Add(new SqlParameter("@Tipo_Incapacidad", TipoIncapacidad));
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Emision", FecEmision));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Inicio", FecInicio));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Final", FecFinal));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));

            if (consulta.Connection.State == ConnectionState.Open)
            {
                consulta.Connection.Close();
            }

            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        #endregion        

        #region Consultas

        public DataSet MCargarTiposIncapacidades()
        {
            consulta = new SqlCommand("Sp_cargar_tipos_incapacidades", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public String TraeIncapacidad(String Cedula)
        {
            String tipoIncapacidad = String.Empty, fecEmision = String.Empty,
                fecInicio = String.Empty, fecFinal = String.Empty, descripcion = String.Empty, cedula = String.Empty, nombre = String.Empty;

            SqlCommand consulta = new SqlCommand();
            consulta = new SqlCommand("Sp_trae_incapacidades", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));

            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                cedula = lector["Cedula_Paciente"].ToString();
                tipoIncapacidad = lector["Tipo_Incapacidad"].ToString();
                fecEmision = lector["Fecha_Emision"].ToString();
                fecInicio = lector["Fecha_Inicio"].ToString();
                fecFinal = lector["Fecha_Final"].ToString();
                descripcion = lector["Descripcion"].ToString();
                nombre = lector["Nombre_completo"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            String respuesta = cedula + "." + tipoIncapacidad + "." + fecEmision + "." + fecInicio + "." + fecFinal + "." + descripcion + "." + nombre;
            return respuesta;
        }

        public DataSet MConsultarTodosIncapacidades()
        {
            consulta = new SqlCommand("Sp_trae_todos_incapacidades", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        #endregion

    }
}
