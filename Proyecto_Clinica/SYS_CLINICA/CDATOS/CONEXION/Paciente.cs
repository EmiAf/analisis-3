﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Paciente
    {
        #region Variables
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;
        #endregion

        #region Constructor
        public Paciente()
        {

        }
        #endregion
        
        #region Insertar Paciente
        public void MInsertarPaciente(string cedula, string nombre, string ape1, string ape2, string genero, string estado_civil, DateTime fecha_nacimiento, string direccion, string telefono, string celular, string correo, string activo)
        {
            consulta = new SqlCommand("Sp_insertar_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));            
            consulta.Parameters.Add(new SqlParameter("@Genero", genero));
            consulta.Parameters.Add(new SqlParameter("@Estado_Civil", estado_civil));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Nacimiento", fecha_nacimiento));
            consulta.Parameters.Add(new SqlParameter("@Direccion", direccion));
            consulta.Parameters.Add(new SqlParameter("@Telefono", telefono));
            consulta.Parameters.Add(new SqlParameter("@Celular", celular));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));
            consulta.Parameters.Add(new SqlParameter("@Activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Actualizar Paciente
        public void MModificarPaciente(string cedula, string nombre, string ape1, string ape2, string genero, string estado_civil, DateTime fecha_nacimiento, string direccion, string telefono, string celular, string correo, string activo)
        {
            consulta = new SqlCommand("Sp_actualizar_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));            
            consulta.Parameters.Add(new SqlParameter("@Genero", genero));
            consulta.Parameters.Add(new SqlParameter("@Estado_Civil", estado_civil));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Nacimiento", fecha_nacimiento));
            consulta.Parameters.Add(new SqlParameter("@Direccion", direccion));
            consulta.Parameters.Add(new SqlParameter("@Telefono", telefono));
            consulta.Parameters.Add(new SqlParameter("@Celular", celular));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));
            consulta.Parameters.Add(new SqlParameter("@Activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public void MModificarPacienteAlternative(string cedula, string nombre, string ape1, string ape2, string genero, string estado_civil, DateTime fecha_nacimiento, string direccion, string telefono, string celular, string correo)
        {
            consulta = new SqlCommand("Sp_actualizar_paciente_alternative", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));
            consulta.Parameters.Add(new SqlParameter("@Genero", genero));
            consulta.Parameters.Add(new SqlParameter("@Estado_Civil", estado_civil));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Nacimiento", fecha_nacimiento));
            consulta.Parameters.Add(new SqlParameter("@Direccion", direccion));
            consulta.Parameters.Add(new SqlParameter("@Telefono", telefono));
            consulta.Parameters.Add(new SqlParameter("@Celular", celular));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Eliminar Paciente
        public void MEliminarPaciente(string activo, string cedula)
        {
            consulta = new SqlCommand("Sp_inhabilitar_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Activo", activo));
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }
        #endregion

        #region Paciente
        public DataSet MConsultarTodosPaciente()
        {
            consulta = new SqlCommand("Sp_trae_todos_pacientes", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public String BuscarPaciente(String Cedula)
        {
            String nomb = String.Empty, ape1 = String.Empty, ape2 = String.Empty;
            consulta = new SqlCommand("Sp_existe_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                nomb = lector["Nombre"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            if (String.IsNullOrEmpty(nomb))
                return "No";
            else
                return "Si";
        }
        
        public String TraePaciente(String Cedula)
        {
            String nomb = String.Empty, tipoSangre = String.Empty, genero = String.Empty, estCivil = String.Empty, fecha = String.Empty, direccion = String.Empty, tel = String.Empty, ced = string.Empty;
            consulta = new SqlCommand("Sp_trae_paciente_x_cedula", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                nomb = lector["nomb"].ToString();
                tipoSangre = lector["Tipo_Sangre"].ToString();
                genero = lector["Genero"].ToString();
                estCivil = lector["Estado_Civil"].ToString();
                fecha = lector["Fecha"].ToString();
                direccion = lector["Direccion"].ToString();
                tel = lector["Celular"].ToString();
                ced = lector["Cedula_Paciente"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            String respuesta = nomb + "." + tipoSangre + "." + genero + "." + estCivil + "." + fecha + "." + direccion + "." + tel + "." + ced;
            return respuesta;
        }
        #endregion
    }
}
