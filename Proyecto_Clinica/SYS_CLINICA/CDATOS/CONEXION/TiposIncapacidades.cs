﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class TiposIncapacidades
    {
        #region Variables
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;
        #endregion

        #region Constructor
        public TiposIncapacidades()
        {

        }
        #endregion
        
        #region Insertar Tipo Incapacidad
        public void MInsertarTipoIncapacidad(string nombre, string descripcion)
        {
            consulta = new SqlCommand("Sp_insertar_tipo_incapacidad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Nombre_Incapacidad", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Actualizar Tipo Incapacidad
        public void MModificarTipoIncapacidad(int idTipoIncapacidad, string nombreIncapacidad, string Descripcion)
        {
            consulta = new SqlCommand("Sp_actualizar_Tipo_Incapacidad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Incapacidad", idTipoIncapacidad));
            consulta.Parameters.Add(new SqlParameter("@Nombre_Incapacidad", nombreIncapacidad));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", Descripcion));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        #endregion

        #region Eliminar Tipo Incapacidad
        public void MEliminarTipoIncapacidad(int IdTipoIncapacidad)
        {
            consulta = new SqlCommand("Sp_inhabilitar_tipo_incapacidad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Tipo_Incapacidad", IdTipoIncapacidad));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }
        #endregion

        #region Consultas       
        
        public DataSet MConsultarTodosTiposIncapacidades()
        {
            consulta = new SqlCommand("Sp_trae_tipos_incapacidades", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        #endregion

    }
}
