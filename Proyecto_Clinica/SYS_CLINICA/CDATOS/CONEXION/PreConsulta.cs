﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class PreConsulta
    {
        #region Variables

        Conexion conexion = new Conexion();
        public static SqlCommand consulta;

        #endregion

        #region PreConsulta

        // Insertar una Preconsulta
        public void AgregarPreConsulta(String Cedula, String Peso, String Estatura, String IMC, String Temperatura, String Presion_Arterial, DateTime Fecha_PreConsulta, String Diagnostico_Enfermera)
        {
            consulta = new SqlCommand("Sp_insertar_preConsulta", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));
            consulta.Parameters.Add(new SqlParameter("@peso", Peso));
            consulta.Parameters.Add(new SqlParameter("@estatura", Estatura));
            consulta.Parameters.Add(new SqlParameter("@imc", IMC));
            consulta.Parameters.Add(new SqlParameter("@temperatura", Temperatura));
            consulta.Parameters.Add(new SqlParameter("@presion", Presion_Arterial));
            consulta.Parameters.Add(new SqlParameter("@fecha", Fecha_PreConsulta));
            consulta.Parameters.Add(new SqlParameter("@diagnostico", Diagnostico_Enfermera));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        // Trae PreConsulta x Paciente
        public DataSet TraePreConsultas(String Cedula)
        {
            consulta = new SqlCommand("Sp_trae_preconsulta_x_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        #endregion
    }
}
