﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CLINICA.CONEXION
{
    public class Cl_Enfermedad
    {
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;

        #region Constr
        public Cl_Enfermedad()
        {

        }

        #endregion

        #region Methods
        public DataSet ConsultaTodasEnferm() 
        {
            consulta = new SqlCommand("Sp_trae_todas_enfermedades", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public void InsertarEnfermedad(string nombre, string descripcion,string activo)
        {
            consulta = new SqlCommand("Sp_insertar_enfermedad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        
        public void EliminarEnfermedad(int Id_Enferm)
        {
            consulta = new SqlCommand("Sp_eliminar_enfermedad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Enfermedad", Id_Enferm));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }

        public void ActualizaEnfermedad(int id_Enf, string nombre, string descripcion) 
        {
            consulta = new SqlCommand("Sp_actualizar_enfermedad", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Enfermedad", id_Enf));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion
    }
}
