﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Bitacora
    {
        #region Variables
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;
        #endregion

        #region Constructor
        public Bitacora()
        {

        }
        #endregion
        
        #region Insertar Bitacora
        public void MInsertarBitacora(string accion, string descripcion, string usuario, DateTime fecha)
        {
            consulta = new SqlCommand("Sp_insertar_bitacora", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Accion", accion));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@Usuario", usuario));
            consulta.Parameters.Add(new SqlParameter("@Fecha", fecha));                        
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();        

        }

        public DataSet MConsultarTodosUsuariosCombo()
        {
            consulta = new SqlCommand("Sp_trae_usuarios_combo", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        #endregion               

        #region Paciente
        public DataSet MConsultarTodosPaciente()
        {
            consulta = new SqlCommand("Sp_trae_todos_pacientes", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public String BuscarPaciente(String Cedula)
        {
            String nomb = String.Empty, ape1 = String.Empty, ape2 = String.Empty;
            consulta = new SqlCommand("Sp_existe_paciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                nomb = lector["Nombre"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            if (String.IsNullOrEmpty(nomb))
                return "No";
            else
                return "Si";
        }
        
        public String TraePaciente(String Cedula)
        {
            String nomb = String.Empty, tipoSangre = String.Empty, genero = String.Empty, estCivil = String.Empty, fecha = String.Empty, direccion = String.Empty, tel = String.Empty, ced = string.Empty;
            consulta = new SqlCommand("Sp_trae_paciente_x_cedula", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                nomb = lector["nomb"].ToString();
                tipoSangre = lector["Tipo_Sangre"].ToString();
                genero = lector["Genero"].ToString();
                estCivil = lector["Estado_Civil"].ToString();
                fecha = lector["Fecha"].ToString();
                direccion = lector["Direccion"].ToString();
                tel = lector["Celular"].ToString();
                ced = lector["Cedula_Paciente"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            String respuesta = nomb + "." + tipoSangre + "." + genero + "." + estCivil + "." + fecha + "." + direccion + "." + tel + "." + ced;
            return respuesta;
        }
        #endregion
    }
}
