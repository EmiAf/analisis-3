﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class ConsultaMedica
    {
        #region Variables

        Conexion conexion = new Conexion();
        public static SqlCommand consulta;

        #endregion

        #region ConsultaMedica

        // Trae Proximo ID
        public int ProximoId()
        {
            String id = String.Empty;
            int proximo = 0;
            consulta = new SqlCommand("Sp_trae_prox_idConsuta", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                id = lector["Id"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            if (String.IsNullOrEmpty(id))
            {
                proximo = 1;
                return proximo;
            }
            else
            {
                proximo = Convert.ToInt32(id);
                return proximo;
            }
        }

        // Guardar Consulta
        public void InsertarConsulta(String Cedula_Paciente, DateTime fecha, String Sintomas, String Diagnostico)
        {
            consulta = new SqlCommand("Sp_insertar_consulta", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", Cedula_Paciente));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Consulta", fecha));
            consulta.Parameters.Add(new SqlParameter("@Sintomas", Sintomas));
            consulta.Parameters.Add(new SqlParameter("@Diagnostico", Diagnostico));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        #endregion
    }
}
