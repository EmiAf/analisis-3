﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Cl_Medicamento
    {
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;

        #region Constructor
        public Cl_Medicamento()
        {

        }
        #endregion

        #region Methods

        public DataSet ConsultaTodosMedicamentos()
        {
            consulta = new SqlCommand("Sp_trae_todos_medicamentos", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        public DataSet TraerMedicamentoTemporal(string Cedula_Paciente)
        {
            consulta = new SqlCommand("Sp_trae_MedicamentosTemporales", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula_Paciente));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        public void InsertarMedicamento(string nombre,string descripcion,string activo)
        {
            consulta = new SqlCommand("Sp_insertar_medicamento", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        public void EliminarMedicamento(int id_Medic)
        {
            consulta = new SqlCommand("Sp_eliminar_medicamento", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Medicamento", id_Medic));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }

        public void EliminarTemporales()
        {
            consulta = new SqlCommand("Sp_eliminar_temporales", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }
        public void ActualizaMedicamento(int id_Medic, string nombre, string descripcion)
        {
            consulta = new SqlCommand("Sp_actualizar_medicamento", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Medicamento", id_Medic));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        public void AgregarMedicamentoTemporal(int Id_Medicamento, String Cedula_Paciente, String descripcion, DateTime fecha, int num_consulta)
        {
            consulta = new SqlCommand("Sp_insertar_medicamentoTemp", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Medicamento", Id_Medicamento));
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", Cedula_Paciente));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@Fecha_medicamento", fecha));
            consulta.Parameters.Add(new SqlParameter("@Num_Consulta", num_consulta));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        public String DescripcionMedicamento(int id)
        {
            String descripcion = String.Empty;
            consulta = new SqlCommand("Sp_buscar_medicamento", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@id", id));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                descripcion = lector["Descripcion"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            return descripcion;
        }
        public void AgregarMedicamentoXPaciente()
        {
            consulta = new SqlCommand("Sp_insertar_medicamentoXpaciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }


        #endregion
    }
}
