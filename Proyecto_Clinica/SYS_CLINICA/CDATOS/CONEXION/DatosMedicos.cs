﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class DatosMedicos
    {
        #region Variables
        Conexion conexion = new Conexion();
        //--public SqlConnection conex;
        public SqlCommand consulta;
        #endregion

        #region Constructor
        public DatosMedicos()
        {

        }
        #endregion
        
        #region Insertar Dato Medico
        public void MInsertarDatoMedico(string cedula, string tipo, string asegurado, string alergia, string desc_alergia, string enfer_cro, string desc_enfer, string num_emergencia, string nomb_parentesco)
        {
            consulta = new SqlCommand("Sp_insertar_datos_medicos", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Tipo_Sangre", tipo));
            consulta.Parameters.Add(new SqlParameter("@Asegurado", asegurado));
            consulta.Parameters.Add(new SqlParameter("@Alergia", alergia));
            consulta.Parameters.Add(new SqlParameter("@Descrip_Alergias", desc_alergia));
            consulta.Parameters.Add(new SqlParameter("@Enfermedad_Cronica", enfer_cro));
            consulta.Parameters.Add(new SqlParameter("@Descrip_Enfermedades", desc_enfer));
            consulta.Parameters.Add(new SqlParameter("@Num_Emergencia", num_emergencia));
            consulta.Parameters.Add(new SqlParameter("@Nomb_Parentesco", nomb_parentesco));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion     

        #region Actualizar Dato Medico
        public void ModificarDatoMedico(string cedula, string tipo_sangre, string asegurado, string alergia, string desc_alergia, string enfermedadCronica, string desc_enfermedad, string numEmergencia, string parentezco)
        {
            consulta = new SqlCommand("Sp_actualizar_datoMedico", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", cedula));
            consulta.Parameters.Add(new SqlParameter("@Tipo_Sangre", tipo_sangre));
            consulta.Parameters.Add(new SqlParameter("@Asegurado", asegurado));
            consulta.Parameters.Add(new SqlParameter("@Alergia", alergia));
            consulta.Parameters.Add(new SqlParameter("@DescripAlergias", desc_alergia));
            consulta.Parameters.Add(new SqlParameter("@EnfermedadCronica", enfermedadCronica));
            consulta.Parameters.Add(new SqlParameter("@DescripEnfermedades", desc_enfermedad));
            consulta.Parameters.Add(new SqlParameter("@NumEmergencia", numEmergencia));
            consulta.Parameters.Add(new SqlParameter("@Nomb_Parentesco", parentezco));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Paciente       

        public String TraeDatosMedicos(String Cedula)
        {
            String tipoSangre = String.Empty, asegurado = String.Empty, 
                alergia = String.Empty, desc_alergia = String.Empty, enfermedad_cronica = String.Empty, desc_enfermedad = String.Empty,
                num_emergencia = String.Empty, parentesco = String.Empty, cedula = String.Empty, nombre = string.Empty;
            
            SqlCommand consulta = new SqlCommand();
            consulta = new SqlCommand("Sp_trae_datos_medicos", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@cedula", Cedula));                       

            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                cedula = lector["Cedula_Paciente"].ToString();
                tipoSangre = lector["Tipo_Sangre"].ToString();
                asegurado = lector["Asegurado"].ToString();
                alergia = lector["Alergia"].ToString();
                desc_alergia = lector["DescripAlergias"].ToString();
                enfermedad_cronica = lector["EnfermedadCronica"].ToString();
                desc_enfermedad = lector["DescripEnfermedades"].ToString();
                num_emergencia = lector["NumEmergencia"].ToString();
                parentesco = lector["Nomb_Parentesco"].ToString();
                nombre = lector["Nombre_completo"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            String respuesta = cedula + "." + tipoSangre + "." + asegurado + "." + alergia + "." + desc_alergia + "." + enfermedad_cronica + "." + desc_enfermedad + "." + num_emergencia + "." + parentesco + "." + nombre;
            return respuesta;
        }

        
        #endregion

        public DataSet ConsultarDatosMedicosPorCedula(string cedula)
        {
            consulta = new SqlCommand("Sp_buscar_DatosMedicos_cedula", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
    }
}
