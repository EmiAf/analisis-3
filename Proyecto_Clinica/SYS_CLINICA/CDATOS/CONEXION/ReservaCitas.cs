﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class ReservaCitas
    {
        #region Variables

        Conexion conexion = new Conexion();
        public static SqlCommand consulta;

        #endregion

        #region ReservaCitas

        public DataSet ConsultaCitaPorCedula(string cedula)
        {
            consulta = new SqlCommand("Sp_buscar_citas_cedula", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula",  cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        // Insertar una cita
        public void ReservarCita(String Cedula, String Nombre, String Telefono, DateTime Fecha_Cita, String Hora)
        {
            consulta = new SqlCommand("Sp_reservar_citas", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", Nombre));
            consulta.Parameters.Add(new SqlParameter("@Telefono", Telefono));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Cita", Fecha_Cita));
            consulta.Parameters.Add(new SqlParameter("@Hora", Hora));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        // Modificar una cita
        public void ModificarCita(int id, String Cedula, String Nombre, String Telefono, DateTime Fecha_Cita, String Hora)
        {
            consulta = new SqlCommand("Sp_modificar_citas", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Reservacion", id));
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", Nombre));
            consulta.Parameters.Add(new SqlParameter("@Telefono", Telefono));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Cita", Fecha_Cita));
            consulta.Parameters.Add(new SqlParameter("@Hora", Hora));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        // Eliminar una cita
        public void EliminarCita(int id)
        {
            consulta = new SqlCommand("Sp_eliminar_citas", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Reservacion", id));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        // Mostrar todas las citas
        public DataSet MostrarCitas()
        {
            consulta = new SqlCommand("Sp_trae_todas_citas", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public DataSet BuscarIdReservacion(string cedula)
        {
            consulta = new SqlCommand("Sp_buscar_Id_reservacion", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }


        // Buscar una cita
        public String BuscarCita(String Condicion, String Cedula, String Nombre, DateTime Fecha_Cita)
        {
            String ced = String.Empty, nomb = String.Empty, tel = String.Empty, fecha = String.Empty, hora = String.Empty, id = String.Empty;
            consulta = new SqlCommand("Sp_buscar_citas", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@condicion", Condicion));
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula));
            consulta.Parameters.Add(new SqlParameter("@Nombre", Nombre));
            consulta.Parameters.Add(new SqlParameter("@Fecha", Fecha_Cita));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                id = lector["Id_Reservacion"].ToString();
                ced = lector["Cedula"].ToString();
                nomb = lector["Nombre"].ToString();
                tel = lector["Telefono"].ToString();
                fecha = lector["Fecha"].ToString();
                hora = lector["Hora"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            String respuesta = ced + "$" + nomb + "$" + tel + "$" + fecha + "$" + hora + "$" + id;
            return respuesta;
        }

        // Procesar una cita
        public void ProcesarCita(string id)
        {
            consulta = new SqlCommand("Sp_procesar_cita", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Reservacion", id));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        // Validar cita
        public String ValidarCita(DateTime Fecha_Cita, String Hora)
        {
            String nomb = String.Empty, resp = String.Empty;
            consulta = new SqlCommand("Sp_valida_espacio_cita", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Fecha", Fecha_Cita));
            consulta.Parameters.Add(new SqlParameter("@Hora", Hora));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                nomb = lector["Nombre"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            if (String.IsNullOrEmpty(nomb))
                return "LIBRE";
            else
                return "OCUPADO";
        }

        #endregion
    }
}
