﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Cl_Examenes
    {
        Conexion conexion = new Conexion();
        public SqlConnection conex;    

        public SqlCommand consulta;

        #region Construct
        public Cl_Examenes()
        {

        }
        #endregion

        #region Methods

        public DataSet ConsultaTodosExamen()
        {
            consulta = new SqlCommand("Sp_trae_todos_examenes", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public void InsertarExamen(String nombre, String descripcion,string activo)
        {
            consulta = new SqlCommand("Sp_insertar_examen", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public void EliminarExamen(int Id_Ex)
        {
            consulta = new SqlCommand("Sp_eliminar_examen", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Examen", Id_Ex));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }

        public void ActualizaExamen(int id_Ex, String nombre, String descripcion)
        {
            consulta = new SqlCommand("Sp_actualizar_examen", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@Id_Examen", id_Ex));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public String DescripcionExamen(int id)
        {
            String descripcion = String.Empty;
            consulta = new SqlCommand("Sp_buscar_examen", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@id", id));
            consulta.Connection.Open();
            SqlDataReader lector = consulta.ExecuteReader();
            while (lector.Read())
            {
                descripcion = lector["Descripcion"].ToString();
            }
            lector.Close();
            consulta.Connection.Close();
            return descripcion;
        }

        public void AgregarExamenXPaciente()
        {
            consulta = new SqlCommand("Sp_insertar_examenXpaciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public DataSet TraeExamenXPaciente(String Cedula_Paciente)
        {
            consulta = new SqlCommand("Sp_trae_examenesXpaciente", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula_Paciente));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        public DataSet TraeExamenTemporal(string Cedula_Paciente)
        {
            consulta = new SqlCommand("Sp_trae_examenesTemporales", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula", Cedula_Paciente));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        public void AgregarExamenTemporal(int Id_Examen, String Cedula_Paciente, String descripcion, DateTime fecha, int num_consulta)
        {
            consulta = new SqlCommand("Sp_insertar_examenTemp", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Id_Examen", Id_Examen));
            consulta.Parameters.Add(new SqlParameter("@Cedula_Paciente", Cedula_Paciente));
            consulta.Parameters.Add(new SqlParameter("@Descripcion", descripcion));
            consulta.Parameters.Add(new SqlParameter("@Fecha_Examen", fecha));
            consulta.Parameters.Add(new SqlParameter("@Num_Consulta", num_consulta));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public DataSet TraeExamenTemporales()
        {
            consulta = new SqlCommand("Sp_traeExaTemporales", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }

        #endregion

    }
}
