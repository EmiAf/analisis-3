﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICA.CONEXION
{
    public class Empleado
    {
        CONEXION.Global global = new CONEXION.Global();
        #region Variables
        Conexion conexion = new Conexion();
        public SqlConnection conex;
        public SqlCommand consulta;

        private string usuario;
        private string contrasenna;
        #endregion

        #region Constructor
        public Empleado()
        {
            usuario = string.Empty;
            contrasenna = string.Empty;
        }
        
        public string Usuario
        {
            get { return this.usuario; }
            set { this.usuario = value; }
        }

        public string Contrasenna
        {
            get { return this.contrasenna; }
            set { this.contrasenna = value; }
        }

        public String Buscar()
        {            
            String Resultado = string.Empty;
            String IdRol = String.Empty, Estado = string.Empty, Nombre = string.Empty, Apellido = string.Empty;
            consulta = new SqlCommand("Sp_login", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Usuario", usuario));
            consulta.Parameters.Add(new SqlParameter("@Pass", contrasenna));
            consulta.Connection.Open();
            SqlDataReader Reg = null;
            Reg = this.consulta.ExecuteReader();

            if (Reg.Read())
            {                
                Resultado = "True";
                IdRol = Reg["Id_Rol"].ToString();
                Estado = Reg["Activo"].ToString();
                Nombre = Reg["Nombre"].ToString();
                Apellido = Reg["Apellido1"].ToString();  
            }
            else
            {
                Resultado = "False";
            }

            consulta.Connection.Close();
            string resul = Resultado + "." + IdRol + "." + Estado + "." + Nombre + "." +Apellido;

            return resul;

        }
        #endregion
        
        #region Insertar Empleado
        public void MInsertarEmpleado(string cedula, int rol, string nombre, string ape1, string ape2, string tel, string correo, string usuario, string contra)
        {
            consulta = new SqlCommand("Sp_insertar_empleado", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Empleado", cedula));
            consulta.Parameters.Add(new SqlParameter("@Id_Rol", rol));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));
            consulta.Parameters.Add(new SqlParameter("@Telefono", tel));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));
            consulta.Parameters.Add(new SqlParameter("@Usuario", usuario));
            consulta.Parameters.Add(new SqlParameter("@Passwd", contra));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }
        #endregion

        #region Actualizar Empleado
        public void MModificarEmpleado(string cedula, int rol, string nombre,string ape1,string ape2,string tel,string correo,string usuario,string contra,string activo)
        {
            consulta = new SqlCommand("Sp_actualizar_empleado", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Empleado", cedula));
            consulta.Parameters.Add(new SqlParameter("@Id_Rol", rol));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));
            consulta.Parameters.Add(new SqlParameter("@Telefono", tel));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));
            consulta.Parameters.Add(new SqlParameter("@Usuario", usuario));
            consulta.Parameters.Add(new SqlParameter("@Passwd", contra));
            consulta.Parameters.Add(new SqlParameter("@Activo", activo));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        public void MModificarEmpleadoAlternative(string cedula, int rol, string nombre, string ape1, string ape2, string tel, string correo, string usuario, string contra)
        {
            consulta = new SqlCommand("Sp_actualizar_empleado_alternative", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.Add(new SqlParameter("@Cedula_Empleado", cedula));
            consulta.Parameters.Add(new SqlParameter("@Id_Rol", rol));
            consulta.Parameters.Add(new SqlParameter("@Nombre", nombre));
            consulta.Parameters.Add(new SqlParameter("@Apellido1", ape1));
            consulta.Parameters.Add(new SqlParameter("@Apellido2", ape2));
            consulta.Parameters.Add(new SqlParameter("@Telefono", tel));
            consulta.Parameters.Add(new SqlParameter("@Correo", correo));
            consulta.Parameters.Add(new SqlParameter("@Usuario", usuario));
            consulta.Parameters.Add(new SqlParameter("@Passwd", contra));            
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();
        }

        #endregion

        #region Eliminar Empleado
        public void MEliminarEmpleado(string cedula)
        {
            consulta = new SqlCommand("Sp_inhabilitar_empleado", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;            
            consulta.Parameters.Add(new SqlParameter("@Cedula_Empleado", cedula));
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            consulta.Connection.Close();

        }
        #endregion

        #region Consultas

        public DataSet MConsultarTodosEmpleados()
        {
            consulta = new SqlCommand("Sp_trae_todos_empleados", conexion.sql);
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Connection.Open();
            consulta.ExecuteNonQuery();
            DataSet ds = new DataSet();
            adaptador.Fill(ds);
            consulta.Connection.Close();
            return (ds);
        }
        #endregion

    }
}
