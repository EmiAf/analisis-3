﻿namespace CLINICA
{
    partial class frm_principal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_principal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrasSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enfermedadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datosPersonalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incapacidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposDeIncapacidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservacionDeCitasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaMédicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preConsultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incapacidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examenesYMedicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasPorPacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bitacorasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sistemaToolStripMenuItem,
            this.catalogosToolStripMenuItem,
            this.citasToolStripMenuItem,
            this.consultaMédicaToolStripMenuItem,
            this.ayudaToolStripMenuItem,
            this.reportesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(942, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarSesionToolStripMenuItem,
            this.cerrasSesionToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sistemaToolStripMenuItem.Text = "Sistema";
            // 
            // iniciarSesionToolStripMenuItem
            // 
            this.iniciarSesionToolStripMenuItem.Name = "iniciarSesionToolStripMenuItem";
            this.iniciarSesionToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.iniciarSesionToolStripMenuItem.Text = "&Iniciar Sesion";
            this.iniciarSesionToolStripMenuItem.Click += new System.EventHandler(this.iniciarSesionToolStripMenuItem_Click);
            // 
            // cerrasSesionToolStripMenuItem
            // 
            this.cerrasSesionToolStripMenuItem.Name = "cerrasSesionToolStripMenuItem";
            this.cerrasSesionToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.cerrasSesionToolStripMenuItem.Text = "Cerra&r Sesion";
            this.cerrasSesionToolStripMenuItem.Click += new System.EventHandler(this.cerrasSesionToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.salirToolStripMenuItem.Text = "&Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // catalogosToolStripMenuItem
            // 
            this.catalogosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.empleadoToolStripMenuItem,
            this.examenToolStripMenuItem,
            this.enfermedadToolStripMenuItem,
            this.medicamentoToolStripMenuItem,
            this.pacienteToolStripMenuItem,
            this.rolToolStripMenuItem,
            this.incapacidadToolStripMenuItem,
            this.tiposDeIncapacidadToolStripMenuItem});
            this.catalogosToolStripMenuItem.Name = "catalogosToolStripMenuItem";
            this.catalogosToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.catalogosToolStripMenuItem.Text = "Catalogos";
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.empleadoToolStripMenuItem.Text = "&Empleados";
            this.empleadoToolStripMenuItem.Click += new System.EventHandler(this.empleadoToolStripMenuItem_Click);
            // 
            // examenToolStripMenuItem
            // 
            this.examenToolStripMenuItem.Name = "examenToolStripMenuItem";
            this.examenToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.examenToolStripMenuItem.Text = "E&xamenes";
            this.examenToolStripMenuItem.Click += new System.EventHandler(this.examenToolStripMenuItem_Click);
            // 
            // enfermedadToolStripMenuItem
            // 
            this.enfermedadToolStripMenuItem.Name = "enfermedadToolStripMenuItem";
            this.enfermedadToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.enfermedadToolStripMenuItem.Text = "En&fermedad";
            this.enfermedadToolStripMenuItem.Click += new System.EventHandler(this.enfermedadToolStripMenuItem_Click);
            // 
            // medicamentoToolStripMenuItem
            // 
            this.medicamentoToolStripMenuItem.Name = "medicamentoToolStripMenuItem";
            this.medicamentoToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.medicamentoToolStripMenuItem.Text = "&Medicamentos";
            this.medicamentoToolStripMenuItem.Click += new System.EventHandler(this.medicamentoToolStripMenuItem_Click);
            // 
            // pacienteToolStripMenuItem
            // 
            this.pacienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datosPersonalesToolStripMenuItem});
            this.pacienteToolStripMenuItem.Name = "pacienteToolStripMenuItem";
            this.pacienteToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.pacienteToolStripMenuItem.Text = "Pacientes";
            this.pacienteToolStripMenuItem.Click += new System.EventHandler(this.pacienteToolStripMenuItem_Click);
            // 
            // datosPersonalesToolStripMenuItem
            // 
            this.datosPersonalesToolStripMenuItem.Name = "datosPersonalesToolStripMenuItem";
            this.datosPersonalesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.datosPersonalesToolStripMenuItem.Text = "&Datos Personales";
            this.datosPersonalesToolStripMenuItem.Click += new System.EventHandler(this.datosPersonalesToolStripMenuItem_Click);
            // 
            // rolToolStripMenuItem
            // 
            this.rolToolStripMenuItem.Name = "rolToolStripMenuItem";
            this.rolToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.rolToolStripMenuItem.Text = "Roles &Usuario";
            this.rolToolStripMenuItem.Click += new System.EventHandler(this.rolToolStripMenuItem_Click);
            // 
            // incapacidadToolStripMenuItem
            // 
            this.incapacidadToolStripMenuItem.Name = "incapacidadToolStripMenuItem";
            this.incapacidadToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.incapacidadToolStripMenuItem.Text = "Incapacidad";
            this.incapacidadToolStripMenuItem.Click += new System.EventHandler(this.incapacidadToolStripMenuItem_Click);
            // 
            // tiposDeIncapacidadToolStripMenuItem
            // 
            this.tiposDeIncapacidadToolStripMenuItem.Name = "tiposDeIncapacidadToolStripMenuItem";
            this.tiposDeIncapacidadToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.tiposDeIncapacidadToolStripMenuItem.Text = "Tipos de Incapacidad";
            this.tiposDeIncapacidadToolStripMenuItem.Click += new System.EventHandler(this.tiposDeIncapacidadToolStripMenuItem_Click);
            // 
            // citasToolStripMenuItem
            // 
            this.citasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reservacionDeCitasToolStripMenuItem});
            this.citasToolStripMenuItem.Name = "citasToolStripMenuItem";
            this.citasToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.citasToolStripMenuItem.Text = "Citas";
            // 
            // reservacionDeCitasToolStripMenuItem
            // 
            this.reservacionDeCitasToolStripMenuItem.Name = "reservacionDeCitasToolStripMenuItem";
            this.reservacionDeCitasToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.reservacionDeCitasToolStripMenuItem.Text = "Reser&vacion de citas";
            this.reservacionDeCitasToolStripMenuItem.Click += new System.EventHandler(this.reservacionDeCitasToolStripMenuItem_Click);
            // 
            // consultaMédicaToolStripMenuItem
            // 
            this.consultaMédicaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preConsultaToolStripMenuItem,
            this.consultaToolStripMenuItem});
            this.consultaMédicaToolStripMenuItem.Name = "consultaMédicaToolStripMenuItem";
            this.consultaMédicaToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.consultaMédicaToolStripMenuItem.Text = "Consulta Médica";
            // 
            // preConsultaToolStripMenuItem
            // 
            this.preConsultaToolStripMenuItem.Name = "preConsultaToolStripMenuItem";
            this.preConsultaToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.preConsultaToolStripMenuItem.Text = "&PreConsulta";
            this.preConsultaToolStripMenuItem.Click += new System.EventHandler(this.preConsultaToolStripMenuItem_Click);
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.consultaToolStripMenuItem.Text = "&Consulta";
            this.consultaToolStripMenuItem.Click += new System.EventHandler(this.consultaToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "&Ayuda";
            this.ayudaToolStripMenuItem.Click += new System.EventHandler(this.ayudaToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultasToolStripMenuItem,
            this.pacientesToolStripMenuItem,
            this.empleadosToolStripMenuItem,
            this.incapacidadesToolStripMenuItem,
            this.examenesYMedicamentosToolStripMenuItem,
            this.medicamentosToolStripMenuItem,
            this.consultasPorPacienteToolStripMenuItem,
            this.citasToolStripMenuItem1,
            this.bitacorasToolStripMenuItem});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.reportesToolStripMenuItem.Text = "Reportes";
            this.reportesToolStripMenuItem.Click += new System.EventHandler(this.reportesToolStripMenuItem_Click);
            // 
            // consultasToolStripMenuItem
            // 
            this.consultasToolStripMenuItem.Name = "consultasToolStripMenuItem";
            this.consultasToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.consultasToolStripMenuItem.Text = "Consultas";
            this.consultasToolStripMenuItem.Click += new System.EventHandler(this.consultasToolStripMenuItem_Click);
            // 
            // pacientesToolStripMenuItem
            // 
            this.pacientesToolStripMenuItem.Name = "pacientesToolStripMenuItem";
            this.pacientesToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.pacientesToolStripMenuItem.Text = "Pacientes";
            this.pacientesToolStripMenuItem.Click += new System.EventHandler(this.pacientesToolStripMenuItem_Click);
            // 
            // empleadosToolStripMenuItem
            // 
            this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
            this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.empleadosToolStripMenuItem.Text = "Empleados";
            this.empleadosToolStripMenuItem.Click += new System.EventHandler(this.empleadosToolStripMenuItem_Click);
            // 
            // incapacidadesToolStripMenuItem
            // 
            this.incapacidadesToolStripMenuItem.Name = "incapacidadesToolStripMenuItem";
            this.incapacidadesToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.incapacidadesToolStripMenuItem.Text = "Incapacidades";
            this.incapacidadesToolStripMenuItem.Click += new System.EventHandler(this.incapacidadesToolStripMenuItem_Click);
            // 
            // examenesYMedicamentosToolStripMenuItem
            // 
            this.examenesYMedicamentosToolStripMenuItem.Name = "examenesYMedicamentosToolStripMenuItem";
            this.examenesYMedicamentosToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.examenesYMedicamentosToolStripMenuItem.Text = "Exámenes";
            this.examenesYMedicamentosToolStripMenuItem.Click += new System.EventHandler(this.examenesYMedicamentosToolStripMenuItem_Click);
            // 
            // medicamentosToolStripMenuItem
            // 
            this.medicamentosToolStripMenuItem.Name = "medicamentosToolStripMenuItem";
            this.medicamentosToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.medicamentosToolStripMenuItem.Text = "Medicamentos";
            this.medicamentosToolStripMenuItem.Click += new System.EventHandler(this.medicamentosToolStripMenuItem_Click);
            // 
            // consultasPorPacienteToolStripMenuItem
            // 
            this.consultasPorPacienteToolStripMenuItem.Name = "consultasPorPacienteToolStripMenuItem";
            this.consultasPorPacienteToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.consultasPorPacienteToolStripMenuItem.Text = "Consultas por Paciente";
            this.consultasPorPacienteToolStripMenuItem.Click += new System.EventHandler(this.consultasPorPacienteToolStripMenuItem_Click);
            // 
            // citasToolStripMenuItem1
            // 
            this.citasToolStripMenuItem1.Name = "citasToolStripMenuItem1";
            this.citasToolStripMenuItem1.Size = new System.Drawing.Size(195, 22);
            this.citasToolStripMenuItem1.Text = "Citas";
            this.citasToolStripMenuItem1.Click += new System.EventHandler(this.citasToolStripMenuItem1_Click);
            // 
            // bitacorasToolStripMenuItem
            // 
            this.bitacorasToolStripMenuItem.Name = "bitacorasToolStripMenuItem";
            this.bitacorasToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.bitacorasToolStripMenuItem.Text = "Bitacoras";
            this.bitacorasToolStripMenuItem.Click += new System.EventHandler(this.bitacorasToolStripMenuItem_Click);
            // 
            // frm_principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CLINICA.Properties.Resources.ConsultoriJuneda;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(942, 467);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frm_principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLINICA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_principal_FormClosing);
            this.Load += new System.EventHandler(this.frm_principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem cerrasSesionToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem catalogosToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem empleadoToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem examenToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem enfermedadToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem medicamentoToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem rolToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem citasToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem reservacionDeCitasToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem consultaMédicaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem preConsultaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem incapacidadToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem datosPersonalesToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem tiposDeIncapacidadToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem iniciarSesionToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incapacidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examenesYMedicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasPorPacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem medicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitacorasToolStripMenuItem;
    }
}

