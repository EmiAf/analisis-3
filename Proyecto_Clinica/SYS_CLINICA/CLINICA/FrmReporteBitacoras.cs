﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class FrmReporteBitacoras : Form
    {
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public FrmReporteBitacoras()
        {
            InitializeComponent();
        }

        private void FrmReporteBitacoras_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_Bitacoras' table. You can move, or remove it, as needed.
            this.CargarComboUsuarios();
        }

        private void CargarComboUsuarios()
        {
            DataSet dsActivo = bitacora.MConsultarTodosUsuariosCombo();
            cb_Usuarios.DataSource = dsActivo.Tables[0];
            cb_Usuarios.DisplayMember = "NombreAp";
            cb_Usuarios.ValueMember = "Nick";
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if ((String.IsNullOrEmpty(cb_Usuarios.SelectedValue.ToString())) || dtpInicio.Value == null || dtpFinal.Value == null)
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                reportViewer1.Visible = true;                
                this.Sp_Reporte_BitacorasTableAdapter.Fill(this.DataSet1.Sp_Reporte_Bitacoras, cb_Usuarios.SelectedValue.ToString(), dtpInicio.Value, dtpFinal.Value);
                this.reportViewer1.RefreshReport();
            }             
        }
    }
}
