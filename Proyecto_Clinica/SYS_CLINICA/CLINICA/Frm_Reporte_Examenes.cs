﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class Frm_Reporte_Examenes : Form
    {
        public Frm_Reporte_Examenes()
        {
            InitializeComponent();
        }

        private void Frm_Reporte_Examenes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_Examenes' table. You can move, or remove it, as needed.
            
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if ((!txtCedula.MaskCompleted) || dtpInicio.Value == null || dtpFinal.Value == null)
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this.Sp_Reporte_ExamenesTableAdapter.Fill(this.DataSet1.Sp_Reporte_Examenes, dtpInicio.Value, dtpFinal.Value, txtCedula.Text);

                this.reportViewer1.RefreshReport();
            }
            
            
        }
    }
}
