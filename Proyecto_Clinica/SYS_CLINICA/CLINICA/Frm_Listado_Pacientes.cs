﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class Frm_Listado_Pacientes : Form
    {
        public Frm_Listado_Pacientes()
        {
            InitializeComponent();
        }

        private void Frm_Listado_Pacientes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_listado_Pacientes' table. You can move, or remove it, as needed.
            this.Sp_Reporte_listado_PacientesTableAdapter.Fill(this.DataSet1.Sp_Reporte_listado_Pacientes);

            this.reportViewer1.RefreshReport();
        }
    }
}
