﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class Frm_Reporte_Historico_Consultas : Form
    {
        public Frm_Reporte_Historico_Consultas()
        {
            InitializeComponent();
        }

        private void Frm_Reporte_Historico_Consultas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_Historico_Consultas' table. You can move, or remove it, as needed.
            
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if ((!txtCedula.MaskCompleted))
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this.Sp_Reporte_Historico_ConsultasTableAdapter.Fill(this.DataSet1.Sp_Reporte_Historico_Consultas, txtCedula.Text);

                this.reportViewer1.RefreshReport();
            }
            
        }
    }
}
