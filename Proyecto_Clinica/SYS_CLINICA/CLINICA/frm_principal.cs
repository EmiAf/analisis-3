﻿using CLINICA.CATALOGOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CLINICA
{
    public partial class frm_principal : Form
    {
        CONEXION.Global global = new CONEXION.Global();
        CONEXION.Cl_Medicamento medicamentos = new CONEXION.Cl_Medicamento();
        public string NomUser;
        public frm_principal()
        {
            InitializeComponent();
            cerrasSesionToolStripMenuItem.Enabled = false;
            catalogosToolStripMenuItem.Visible = false;
            citasToolStripMenuItem.Visible = false;
            consultaMédicaToolStripMenuItem.Visible = false;
            reportesToolStripMenuItem.Visible = false;
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_empleado em = new frm_empleado();
            em.Show();
            em.MdiParent = this;
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //for (int i = -1000; i < this.MdiChildren.Length; i++)
            //{
            //    Form FormHijoActivo = (Form)ActiveMdiChild;
            //    if (FormHijoActivo != null) FormHijoActivo.Close();
            //}
            //frm_paciente pa = new frm_paciente();
            //pa.Show();
            //pa.MdiParent = this;
        }

        private void rolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Rol s = new Rol();
            s.Show();
            s.MdiParent = this;
        }

        private void enfermedadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_enfermedad enf = new frm_enfermedad();
            enf.Show();
            enf.MdiParent = this;
        }

        private void examenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_examen examen = new frm_examen();
            examen.Show();
            examen.MdiParent = this;
        }

        private void medicamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_medicamento medic = new frm_medicamento();
            //Form mantenimiento2 = new Mantenimiento_Operadores();
            medic.MdiParent = this;
            // Para mostrarlo maximizado:
            //medic.WindowState = FormWindowState.Maximized;
            
            medic.Show();
        }

        private void datosMedicosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }

            frmDatosMedicos datosmed = new frmDatosMedicos();
            datosmed.Show();
            datosmed.MdiParent = this;
        }

        private void reservacionDeCitasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }

            frmCitas citas = new frmCitas();
            citas.Show();
            citas.MdiParent = this;
        }

        private void preConsultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }

            frm_preConsulta preConsulta = new frm_preConsulta();
            preConsulta.Show();
            preConsulta.MdiParent = this;
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }


            frm_consulta consulta = new frm_consulta();
            consulta.Show();
            consulta.MdiParent = this;
        }

        private void incapacidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_incapacidad incap = new frm_incapacidad();
            incap.MdiParent = this;
            incap.Show();
        }

        private void consultaDeCitasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }

            frm_consultaCitas citas = new frm_consultaCitas();
            citas.Show();
            citas.MdiParent = this;
        }

        private void datosPersonalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_paciente pa = new frm_paciente();
            pa.Show();
            pa.MdiParent = this;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=4 C:\\Comercio_electronico.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

        private void tiposDeIncapacidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            FrmTipos FT = new FrmTipos();
            FT.Show();
            FT.MdiParent = this;            
        }

        private void iniciarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            frm_Login login = new frm_Login();
            login.MdiParent = this;
            login.txtPasswd.Clear();
            login.txtUsuario.Clear();
            login.txtUsuario.Focus();
            login.Show();
        }

        private void cerrasSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //-global.globalVar = string.Empty;
            cerrasSesionToolStripMenuItem.Enabled = false;
            catalogosToolStripMenuItem.Visible = false;
            citasToolStripMenuItem.Visible = false;
            consultaMédicaToolStripMenuItem.Visible = false;
            iniciarSesionToolStripMenuItem.Enabled = true;
            reportesToolStripMenuItem.Visible = false;
        }

        private void frm_principal_Load(object sender, EventArgs e)
        {
            frm_Login login = new frm_Login();
            this.NomUser = login.NombreUsuario;
        }

        private void frm_principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DialogResult dialogResult = MessageBox.Show("No se guardaran los cambios, desea salir?", "Consulta.", MessageBoxButtons.YesNo);
            //if (dialogResult == DialogResult.Yes)
            //{
                
            medicamentos.EliminarTemporales();
            //}

        }

        private void consultasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Consultas_Realiazadas em = new Consultas_Realiazadas();
            em.Show();
            em.MdiParent = this;
        }

        private void pacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Listado_Pacientes em = new Frm_Listado_Pacientes();
            em.Show();
            em.MdiParent = this;
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Listado_Empleados em = new Frm_Listado_Empleados();
            em.Show();
            em.MdiParent = this;
        }

        private void incapacidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Incapacidades em = new Frm_Incapacidades();
            em.Show();
            em.MdiParent = this;
        }

        private void examenesYMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Reporte_Examenes em = new Frm_Reporte_Examenes();
            em.Show();
            em.MdiParent = this;
        }

        private void medicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Reporte_Medicamentos em = new Frm_Reporte_Medicamentos();
            em.Show();
            em.MdiParent = this;
        }

        private void consultasPorPacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Reporte_Historico_Consultas em = new Frm_Reporte_Historico_Consultas();
            em.Show();
            em.MdiParent = this;
        }

        private void citasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            Frm_Reporte_Citas em = new Frm_Reporte_Citas();
            em.Show();
            em.MdiParent = this;
        }

        private void bitacorasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = -1000; i < this.MdiChildren.Length; i++)
            {
                //this.MdiChildren[i].WindowState = FormWindowState.Minimized;
                Form FormHijoActivo = (Form)ActiveMdiChild;
                if (FormHijoActivo != null) FormHijoActivo.Close();
            }
            FrmReporteBitacoras em = new FrmReporteBitacoras();
            em.Show();
            em.MdiParent = this;
        }

        private void reportesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultasPorMedicosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
