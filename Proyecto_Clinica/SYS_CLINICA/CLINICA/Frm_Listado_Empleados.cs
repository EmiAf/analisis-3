﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class Frm_Listado_Empleados : Form
    {
        public Frm_Listado_Empleados()
        {
            InitializeComponent();
        }

        private void Frm_Listado_Empleados_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_listado_Empleados' table. You can move, or remove it, as needed.
            this.Sp_Reporte_listado_EmpleadosTableAdapter.Fill(this.DataSet1.Sp_Reporte_listado_Empleados);

            this.reportViewer1.RefreshReport();
        }
    }
}
