﻿namespace CLINICA
{
    partial class Frm_Listado_Pacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.Sp_Reporte_listado_PacientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSet1 = new CLINICA.DataSet1();
            this.label14 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Sp_Reporte_listado_PacientesTableAdapter = new CLINICA.DataSet1TableAdapters.Sp_Reporte_listado_PacientesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.Sp_Reporte_listado_PacientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // Sp_Reporte_listado_PacientesBindingSource
            // 
            this.Sp_Reporte_listado_PacientesBindingSource.DataMember = "Sp_Reporte_listado_Pacientes";
            this.Sp_Reporte_listado_PacientesBindingSource.DataSource = this.DataSet1;
            // 
            // DataSet1
            // 
            this.DataSet1.DataSetName = "DataSet1";
            this.DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(376, 24);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(219, 20);
            this.label14.TabIndex = 77;
            this.label14.Text = "LISTADO DE PACIENTES";
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.Sp_Reporte_listado_PacientesBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "CLINICA.Reportes.Report_Listado_Pacientes.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 81);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(965, 441);
            this.reportViewer1.TabIndex = 78;
            // 
            // Sp_Reporte_listado_PacientesTableAdapter
            // 
            this.Sp_Reporte_listado_PacientesTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_Listado_Pacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(989, 548);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.label14);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Listado_Pacientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Listado_Pacientes";
            this.Load += new System.EventHandler(this.Frm_Listado_Pacientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Sp_Reporte_listado_PacientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource Sp_Reporte_listado_PacientesBindingSource;
        private DataSet1 DataSet1;
        private DataSet1TableAdapters.Sp_Reporte_listado_PacientesTableAdapter Sp_Reporte_listado_PacientesTableAdapter;
    }
}