﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class Frm_Incapacidades : Form
    {
        public Frm_Incapacidades()
        {
            InitializeComponent();
        }

        private void Frm_Incapacidades_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Sp_Reporte_Incapacidades' table. You can move, or remove it, as needed.
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (dtpInicio.Value == null || dtpFinal.Value == null)
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                reportViewer1.Visible = true;
                this.Sp_Reporte_IncapacidadesTableAdapter.Fill(this.DataSet1.Sp_Reporte_Incapacidades, dtpInicio.Value, dtpFinal.Value);
                this.reportViewer1.RefreshReport();
            }
            
        }
    }
}
