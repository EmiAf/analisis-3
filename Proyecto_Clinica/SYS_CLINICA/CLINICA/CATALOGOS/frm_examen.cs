﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_examen : Form
    {
        private DataSet dsGrid;
        CONEXION.Cl_Examenes Examen = new CONEXION.Cl_Examenes();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        public frm_examen()
        {
            InitializeComponent();
        }

        private void cargarExamenes()
        {
            try
            {
                dsGrid = Examen.ConsultaTodosExamen();
                dgvExamenes.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar los datos", " Cargar Examenes ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void Limpiar()
        {
            txtCodigo.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtDescripcion.Text = string.Empty;            
        }

        public void habilitar(bool ingresar,bool actualizar)
        {
            this.btn_Ingresar.Enabled = ingresar;
            this.btn_Actualizar.Enabled = actualizar;
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDescripcion.Text) || string.IsNullOrEmpty(txtNombre.Text))
                {
                    MessageBox.Show("Exiten campos vacios, Por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Examen.InsertarExamen(txtNombre.Text,txtDescripcion.Text,"1");
                    bitacora.MInsertarBitacora("Insertar", "Insercion Examen", NomUser, DateTime.Today);
                    cargarExamenes();
                    MessageBox.Show("El exámen se guardó con éxto", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                    txtNombre.Focus();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al guardar, por favor verificar.", "Insertar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Examen.EliminarExamen(Convert.ToInt32(txtCodigo.Text));
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Examen", NomUser, DateTime.Today);
                    cargarExamenes();
                    MessageBox.Show("El exámen se eliminó con éxito", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                    txtNombre.Focus();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al eliminar, Por favor digite un código", " Eliminar datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
            habilitar(true, false);
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDescripcion.Text) || string.IsNullOrEmpty(txtCodigo.Text) || string.IsNullOrEmpty(txtNombre.Text))
                {
                    MessageBox.Show("Exiten campos vacios, Por favor verificar.", " Validacion de datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Examen.ActualizaExamen(Int32.Parse(txtCodigo.Text), txtNombre.Text, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Examen", NomUser, DateTime.Today);
                    cargarExamenes();
                    MessageBox.Show("El exámen se modificó con éxito", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                    txtNombre.Focus();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al modificar, Por favor verificar.", "Modificar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void frm_examen_Load(object sender, EventArgs e)
        {
            cargarExamenes();
            this.NomUser = Session.UserSesion;
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void dgvExamenes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtCodigo.Text = dgvExamenes.CurrentRow.Cells[0].Value.ToString();
                txtNombre.Text = dgvExamenes.CurrentRow.Cells[1].Value.ToString();
                txtDescripcion.Text = dgvExamenes.CurrentRow.Cells[2].Value.ToString();
                this.habilitar(false, true);
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione una celda válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=11 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

    }
}
