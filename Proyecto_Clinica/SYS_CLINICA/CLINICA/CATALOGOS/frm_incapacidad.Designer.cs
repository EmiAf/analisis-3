﻿namespace CLINICA.CATALOGOS
{
    partial class frm_incapacidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_incapacidad));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_Dias_Incapacidad = new System.Windows.Forms.Label();
            this.lbl_Nombre = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_Enca_Dias = new System.Windows.Forms.Label();
            this.txtCedula = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtp_FecFinal = new System.Windows.Forms.DateTimePicker();
            this.dtp_FecInicio = new System.Windows.Forms.DateTimePicker();
            this.dtp_FecEmision = new System.Windows.Forms.DateTimePicker();
            this.cbxTipoIncapacidad = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_Incapacidades = new System.Windows.Forms.DataGridView();
            this.Cedula_Paciente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_completo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo_Incapacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Emision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Inicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Incapacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dias_Incapacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.lkLblMenu = new System.Windows.Forms.LinkLabel();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.txtCedBusqueda = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Ingresar = new System.Windows.Forms.Button();
            this.btn_Actualizar = new System.Windows.Forms.Button();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictHelp = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Incapacidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_Dias_Incapacidad);
            this.groupBox1.Controls.Add(this.lbl_Nombre);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lbl_Enca_Dias);
            this.groupBox1.Controls.Add(this.txtCedula);
            this.groupBox1.Controls.Add(this.txtDescripcion);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtp_FecFinal);
            this.groupBox1.Controls.Add(this.dtp_FecInicio);
            this.groupBox1.Controls.Add(this.dtp_FecEmision);
            this.groupBox1.Controls.Add(this.cbxTipoIncapacidad);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 314);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(715, 206);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información de Incapacidad";
            // 
            // lbl_Dias_Incapacidad
            // 
            this.lbl_Dias_Incapacidad.AutoSize = true;
            this.lbl_Dias_Incapacidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Dias_Incapacidad.Location = new System.Drawing.Point(468, 174);
            this.lbl_Dias_Incapacidad.Name = "lbl_Dias_Incapacidad";
            this.lbl_Dias_Incapacidad.Size = new System.Drawing.Size(18, 16);
            this.lbl_Dias_Incapacidad.TabIndex = 60;
            this.lbl_Dias_Incapacidad.Text = "--";
            this.lbl_Dias_Incapacidad.Visible = false;
            // 
            // lbl_Nombre
            // 
            this.lbl_Nombre.AutoSize = true;
            this.lbl_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Nombre.Location = new System.Drawing.Point(453, 34);
            this.lbl_Nombre.Name = "lbl_Nombre";
            this.lbl_Nombre.Size = new System.Drawing.Size(18, 16);
            this.lbl_Nombre.TabIndex = 58;
            this.lbl_Nombre.Text = "--";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(302, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 17);
            this.label10.TabIndex = 57;
            this.label10.Text = "Nombre del Paciente:";
            // 
            // lbl_Enca_Dias
            // 
            this.lbl_Enca_Dias.AutoSize = true;
            this.lbl_Enca_Dias.Location = new System.Drawing.Point(302, 174);
            this.lbl_Enca_Dias.Name = "lbl_Enca_Dias";
            this.lbl_Enca_Dias.Size = new System.Drawing.Size(140, 17);
            this.lbl_Enca_Dias.TabIndex = 59;
            this.lbl_Enca_Dias.Text = "Días de Incapacidad:";
            this.lbl_Enca_Dias.Visible = false;
            // 
            // txtCedula
            // 
            this.txtCedula.AutoSize = true;
            this.txtCedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCedula.Location = new System.Drawing.Point(100, 34);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(18, 16);
            this.txtCedula.TabIndex = 56;
            this.txtCedula.Text = "--";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(428, 107);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(242, 57);
            this.txtDescripcion.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 34);
            this.label2.TabIndex = 54;
            this.label2.Text = "Descripcion\r\no motivo :";
            // 
            // dtp_FecFinal
            // 
            this.dtp_FecFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_FecFinal.Location = new System.Drawing.Point(124, 159);
            this.dtp_FecFinal.Name = "dtp_FecFinal";
            this.dtp_FecFinal.Size = new System.Drawing.Size(141, 23);
            this.dtp_FecFinal.TabIndex = 53;
            // 
            // dtp_FecInicio
            // 
            this.dtp_FecInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_FecInicio.Location = new System.Drawing.Point(124, 116);
            this.dtp_FecInicio.Name = "dtp_FecInicio";
            this.dtp_FecInicio.Size = new System.Drawing.Size(142, 23);
            this.dtp_FecInicio.TabIndex = 52;
            // 
            // dtp_FecEmision
            // 
            this.dtp_FecEmision.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_FecEmision.Location = new System.Drawing.Point(124, 73);
            this.dtp_FecEmision.Name = "dtp_FecEmision";
            this.dtp_FecEmision.Size = new System.Drawing.Size(142, 23);
            this.dtp_FecEmision.TabIndex = 51;
            this.dtp_FecEmision.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // cbxTipoIncapacidad
            // 
            this.cbxTipoIncapacidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoIncapacidad.FormattingEnabled = true;
            this.cbxTipoIncapacidad.Location = new System.Drawing.Point(428, 71);
            this.cbxTipoIncapacidad.Name = "cbxTipoIncapacidad";
            this.cbxTipoIncapacidad.Size = new System.Drawing.Size(210, 24);
            this.cbxTipoIncapacidad.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(302, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 17);
            this.label9.TabIndex = 43;
            this.label9.Text = "Tipo Incapacidad:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 17);
            this.label7.TabIndex = 40;
            this.label7.Text = "Fecha Final:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 38;
            this.label6.Text = "Fecha Inicio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Fecha Emision:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "Cédula:";
            // 
            // dgv_Incapacidades
            // 
            this.dgv_Incapacidades.AllowUserToAddRows = false;
            this.dgv_Incapacidades.AllowUserToDeleteRows = false;
            this.dgv_Incapacidades.AllowUserToOrderColumns = true;
            this.dgv_Incapacidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Incapacidades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cedula_Paciente,
            this.Nombre_completo,
            this.Tipo_Incapacidad,
            this.Fecha_Emision,
            this.Fecha_Inicio,
            this.Fecha_Final,
            this.Descripcion,
            this.Id_Incapacidad,
            this.Dias_Incapacidad});
            this.dgv_Incapacidades.Location = new System.Drawing.Point(31, 89);
            this.dgv_Incapacidades.Name = "dgv_Incapacidades";
            this.dgv_Incapacidades.ReadOnly = true;
            this.dgv_Incapacidades.Size = new System.Drawing.Size(715, 162);
            this.dgv_Incapacidades.TabIndex = 56;
            this.dgv_Incapacidades.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Incapacidades_CellClick);
            this.dgv_Incapacidades.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Cedula_Paciente
            // 
            this.Cedula_Paciente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Cedula_Paciente.DataPropertyName = "Cedula_Paciente";
            this.Cedula_Paciente.HeaderText = "Cedula";
            this.Cedula_Paciente.Name = "Cedula_Paciente";
            this.Cedula_Paciente.ReadOnly = true;
            // 
            // Nombre_completo
            // 
            this.Nombre_completo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nombre_completo.DataPropertyName = "Nombre_completo";
            this.Nombre_completo.HeaderText = "Nombre";
            this.Nombre_completo.Name = "Nombre_completo";
            this.Nombre_completo.ReadOnly = true;
            // 
            // Tipo_Incapacidad
            // 
            this.Tipo_Incapacidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tipo_Incapacidad.DataPropertyName = "Tipo_Incapacidad";
            this.Tipo_Incapacidad.HeaderText = "Tipo de Incapacidad";
            this.Tipo_Incapacidad.Name = "Tipo_Incapacidad";
            this.Tipo_Incapacidad.ReadOnly = true;
            this.Tipo_Incapacidad.Visible = false;
            // 
            // Fecha_Emision
            // 
            this.Fecha_Emision.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Fecha_Emision.DataPropertyName = "Fecha_Emision";
            this.Fecha_Emision.HeaderText = "Fecha de Emisión";
            this.Fecha_Emision.Name = "Fecha_Emision";
            this.Fecha_Emision.ReadOnly = true;
            // 
            // Fecha_Inicio
            // 
            this.Fecha_Inicio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Fecha_Inicio.DataPropertyName = "Fecha_Inicio";
            this.Fecha_Inicio.HeaderText = "Fecha de Inicio";
            this.Fecha_Inicio.Name = "Fecha_Inicio";
            this.Fecha_Inicio.ReadOnly = true;
            // 
            // Fecha_Final
            // 
            this.Fecha_Final.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Fecha_Final.DataPropertyName = "Fecha_Final";
            this.Fecha_Final.HeaderText = "Fecha Final";
            this.Fecha_Final.Name = "Fecha_Final";
            this.Fecha_Final.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // Id_Incapacidad
            // 
            this.Id_Incapacidad.DataPropertyName = "Id_Incapacidad";
            this.Id_Incapacidad.HeaderText = "Id_Incapacidad";
            this.Id_Incapacidad.Name = "Id_Incapacidad";
            this.Id_Incapacidad.ReadOnly = true;
            this.Id_Incapacidad.Visible = false;
            // 
            // Dias_Incapacidad
            // 
            this.Dias_Incapacidad.DataPropertyName = "Dias_Incapacidad";
            this.Dias_Incapacidad.HeaderText = "Dias_Incapacidad";
            this.Dias_Incapacidad.Name = "Dias_Incapacidad";
            this.Dias_Incapacidad.ReadOnly = true;
            this.Dias_Incapacidad.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(253, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(272, 20);
            this.label3.TabIndex = 53;
            this.label3.Text = "INCAPACIDADES REALIZADAS";
            // 
            // lkLblMenu
            // 
            this.lkLblMenu.AutoSize = true;
            this.lkLblMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkLblMenu.Location = new System.Drawing.Point(728, 558);
            this.lkLblMenu.Name = "lkLblMenu";
            this.lkLblMenu.Size = new System.Drawing.Size(50, 18);
            this.lkLblMenu.TabIndex = 68;
            this.lkLblMenu.TabStop = true;
            this.lkLblMenu.Text = "Cerrar";
            this.lkLblMenu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkLblMenu_LinkClicked);
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_buscar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_buscar.Image = global::CLINICA.Properties.Resources.buscarPreConsulta;
            this.btn_buscar.Location = new System.Drawing.Point(309, 258);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(54, 40);
            this.btn_buscar.TabIndex = 77;
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // txtCedBusqueda
            // 
            this.txtCedBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCedBusqueda.Location = new System.Drawing.Point(166, 268);
            this.txtCedBusqueda.Mask = "0-0000-0000";
            this.txtCedBusqueda.Name = "txtCedBusqueda";
            this.txtCedBusqueda.Size = new System.Drawing.Size(128, 22);
            this.txtCedBusqueda.TabIndex = 76;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(28, 271);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 16);
            this.label4.TabIndex = 75;
            this.label4.Text = "Cédula del paciente:";
            // 
            // btn_Ingresar
            // 
            this.btn_Ingresar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Ingresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_Ingresar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_Ingresar.Image = global::CLINICA.Properties.Resources.Accept_icon;
            this.btn_Ingresar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Ingresar.Location = new System.Drawing.Point(224, 526);
            this.btn_Ingresar.Name = "btn_Ingresar";
            this.btn_Ingresar.Size = new System.Drawing.Size(90, 53);
            this.btn_Ingresar.TabIndex = 69;
            this.btn_Ingresar.Text = "Ingresar";
            this.btn_Ingresar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Ingresar.UseVisualStyleBackColor = true;
            this.btn_Ingresar.Click += new System.EventHandler(this.btn_Ingresar_Click);
            // 
            // btn_Actualizar
            // 
            this.btn_Actualizar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Actualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_Actualizar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_Actualizar.Image = global::CLINICA.Properties.Resources.actualizar_restaure_agt_icono_7628_32;
            this.btn_Actualizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Actualizar.Location = new System.Drawing.Point(334, 526);
            this.btn_Actualizar.Name = "btn_Actualizar";
            this.btn_Actualizar.Size = new System.Drawing.Size(90, 53);
            this.btn_Actualizar.TabIndex = 70;
            this.btn_Actualizar.Text = "Actualizar";
            this.btn_Actualizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Actualizar.UseVisualStyleBackColor = true;
            this.btn_Actualizar.Click += new System.EventHandler(this.btn_Actualizar_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_limpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_limpiar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_limpiar.Image = global::CLINICA.Properties.Resources.cambio_de_cepillo_de_escoba_de_barrer_claro_icono_5768_32;
            this.btn_limpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_limpiar.Location = new System.Drawing.Point(442, 526);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(90, 53);
            this.btn_limpiar.TabIndex = 72;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CLINICA.Properties.Resources.misc_tutorial_icon;
            this.pictureBox1.Location = new System.Drawing.Point(109, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(89, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 57;
            this.pictureBox1.TabStop = false;
            // 
            // pictHelp
            // 
            this.pictHelp.Image = ((System.Drawing.Image)(resources.GetObject("pictHelp.Image")));
            this.pictHelp.Location = new System.Drawing.Point(724, 3);
            this.pictHelp.Name = "pictHelp";
            this.pictHelp.Size = new System.Drawing.Size(60, 37);
            this.pictHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictHelp.TabIndex = 78;
            this.pictHelp.TabStop = false;
            this.pictHelp.Click += new System.EventHandler(this.pictHelp_Click);
            // 
            // frm_incapacidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(786, 596);
            this.Controls.Add(this.pictHelp);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.txtCedBusqueda);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_Ingresar);
            this.Controls.Add(this.btn_Actualizar);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.lkLblMenu);
            this.Controls.Add(this.dgv_Incapacidades);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_incapacidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Incapacidad";
            this.Load += new System.EventHandler(this.frm_incapacidad_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Incapacidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHelp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbxTipoIncapacidad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_FecEmision;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgv_Incapacidades;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_FecFinal;
        private System.Windows.Forms.DateTimePicker dtp_FecInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel lkLblMenu;
        private System.Windows.Forms.Button btn_Ingresar;
        private System.Windows.Forms.Button btn_Actualizar;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.MaskedTextBox txtCedBusqueda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Nombre;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txtCedula;
        private System.Windows.Forms.Label lbl_Dias_Incapacidad;
        private System.Windows.Forms.Label lbl_Enca_Dias;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cedula_Paciente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_completo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_Incapacidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Emision;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Inicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Final;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Incapacidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dias_Incapacidad;
        private System.Windows.Forms.PictureBox pictHelp;
    }
}