﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace CLINICA.CATALOGOS
{
    public partial class FrmTipos : Form
    {
        CONEXION.TiposIncapacidades tipos = new CONEXION.TiposIncapacidades();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        private DataSet dsGrid;
        public int Codigo;

        public FrmTipos()
        {
            InitializeComponent();
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    tipos.MInsertarTipoIncapacidad(txtNombre.Text,txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Insertar", "Insercion Tipo Incapacidad", NomUser, DateTime.Today);
                    MessageBox.Show("Tipo de Incapacidad ingresado con éxito", "Ingreso Tipo Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Limpiar();
                    this.CargarGridTiposIncapacidades();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al registrar Tipo Incapacidad. Verifique nuevamente.", "Ingreso Tipo Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpiar()
        {
            txtNombre.Clear();
            txtDescripcion.Clear();
            btn_Ingresar.Enabled = true;
            btn_Actualizar.Enabled = false;
            btn_eliminar.Enabled = false;
        }

        private void CargarGridTiposIncapacidades()
        {
            try
            {
                dsGrid = tipos.MConsultarTodosTiposIncapacidades();
                dgv_tiposIncapacidades.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void FrmTipos_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            this.CargarGridTiposIncapacidades();
            btn_Actualizar.Enabled = false;
            btn_eliminar.Enabled = false;
        }

        private void dgv_tiposIncapacidades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0 || e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 3) && e.RowIndex >= 0)
            {
                try
                {
                    Codigo = 0; //Limpiando la variable codigo
                    txtNombre.Text = dgv_tiposIncapacidades.CurrentRow.Cells[1].Value.ToString();
                    txtDescripcion.Text = dgv_tiposIncapacidades.CurrentRow.Cells[2].Value.ToString();
                    Codigo = Convert.ToInt32(dgv_tiposIncapacidades.CurrentRow.Cells[0].Value.ToString());
                    btn_Actualizar.Enabled = true;
                    btn_eliminar.Enabled = true;
                    btn_Ingresar.Enabled = false;
                }
                catch (System.Exception excep)
                {
                    MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    tipos.MModificarTipoIncapacidad(Codigo, txtNombre.Text, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Tipo Incapacidad", NomUser, DateTime.Today);
                    CargarGridTiposIncapacidades();
                    Limpiar();
                    MessageBox.Show("Tipo Incapacidad editada", "Actualización Tipo Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al editar Tipo Incapacidad. Verifique nuevamente.", "Actualización Tipo Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Codigo == null)
                {
                    MessageBox.Show("Elija Tipo de Incapacidad a inhabilitar. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    tipos.MEliminarTipoIncapacidad(Codigo);
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Tipo Incapacidad", NomUser, DateTime.Today);
                    CargarGridTiposIncapacidades();
                    MessageBox.Show("Tipo de Incapacidad Inhabilitado", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Limpiar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al inhabilitar. Verifique nuevamente.", "Inhabilitar Tipo Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.Limpiar();
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=30 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

    }
}
