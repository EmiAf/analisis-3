﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_preConsulta : Form
    {
        CONEXION.PreConsulta pre = new CONEXION.PreConsulta();
        CONEXION.DatosMedicos datoMed = new CONEXION.DatosMedicos();
        CONEXION.Paciente paciente = new CONEXION.Paciente();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;

        public frm_preConsulta()
        {
            InitializeComponent();
        }

        public void Limpiar()
        {
            txtCedulaPaciente.Clear();
            txtPeso.Clear();
            txtEstatura.Clear();
            lbl_IMC.Text = "--";
            txtTemperatura.Clear();
            txtPresionArterial.Clear();
            txtDiagnostico.Clear();
        }

        private void frm_preConsulta_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            CargarComboTipoSangre();
            CargarComboAsegurado();
            CargarComboAlergias();
            CargarComboEnfermedades();
        }

        private void btnGuardarPreconsulta_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtPeso.Text) || String.IsNullOrEmpty(txtEstatura.Text) || String.IsNullOrEmpty(txtDiagnostico.Text) || String.IsNullOrEmpty(lbl_IMC.Text) || String.IsNullOrEmpty(txtPresionArterial.Text) || String.IsNullOrEmpty(txtTemperatura.Text))
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DataSet dataS;
                dataS = datoMed.ConsultarDatosMedicosPorCedula(txtCedulaPaciente.Text);
                if (dataS.Tables[0].Rows.Count > 0)
                {
                    DateTime fecha = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    String cedula = txtCedulaPaciente.Text, peso = txtPeso.Text, estatura = txtEstatura.Text, imc = lbl_IMC.Text, temperatura = txtTemperatura.Text;
                    String presion = txtPresionArterial.Text, diagnostico = txtDiagnostico.Text;
                    pre.AgregarPreConsulta(cedula, peso, estatura, imc, temperatura, presion, fecha, diagnostico);
                    bitacora.MInsertarBitacora("Insertar", "Insercion Preconsulta", NomUser, DateTime.Today);
                    MessageBox.Show("La PreConsulta se guardó con exito", "Ingreso PreConsulta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    this.Close(); 
                }
                else
                {
                    MessageBox.Show("Deben existir datos medicos para este paciente, si no se podrá ingresar la Pre Consulta.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
              
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxEnfer_Cronicas.SelectedIndex == 1)
            {
                txt_desc_enfermedades.Enabled = false;
            }
            else
            {
                txt_desc_enfermedades.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                datoMed.MInsertarDatoMedico(txt_CP.Text, cbxTipoSangre.SelectedItem.ToString(), cbxAsegurado.SelectedItem.ToString(), cbxAlergias.SelectedItem.ToString(), txt_desc_alergias.Text, cbxEnfer_Cronicas.SelectedItem.ToString(), txt_desc_enfermedades.Text, txt_num_emergencia.Text, txt_parentesco.Text);
                bitacora.MInsertarBitacora("Insertar", "Insercion Dato Medico", NomUser, DateTime.Today);
                MessageBox.Show("Dato Medico ingresado con éxito", "Ingreso Datos Médicos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarDatosMedicos();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al registrar Dato Medico. Verifique nuevamente.", "Ingreso datos medicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        private void CargarComboAsegurado()
        {
            cbxAsegurado.Items.Add("Si");
            cbxAsegurado.Items.Add("No");
        }
        private void CargarComboAlergias()
        {
            cbxAlergias.Items.Add("Si");
            cbxAlergias.Items.Add("No");
        }
        private void CargarComboTipoSangre()
        {            
            cbxTipoSangre.Items.Add("A+");
            cbxTipoSangre.Items.Add("A-");
            cbxTipoSangre.Items.Add("B+");
            cbxTipoSangre.Items.Add("B-");
            cbxTipoSangre.Items.Add("AB+");
            cbxTipoSangre.Items.Add("AB-");
            cbxTipoSangre.Items.Add("O+");
            cbxTipoSangre.Items.Add("O-");
        }
        private void CargarComboEnfermedades()
        {
            cbxEnfer_Cronicas.Items.Add("Si");
            cbxEnfer_Cronicas.Items.Add("No");
        }       
        private void LimpiarDatosMedicos()
        {
            txt_nombreCompleto.Text = "--";
            txt_CP.Text = "--";            
            cbxAlergias.SelectedIndex = -1;
            cbxTipoSangre.SelectedIndex = -1;
            cbxAsegurado.SelectedIndex = -1;
            txt_desc_alergias.Clear();
            cbxEnfer_Cronicas.SelectedIndex = -1;
            txt_desc_enfermedades.Clear();
            txt_parentesco.Clear();
            txt_num_emergencia.Clear();
        }
        private void btn_limpiar_datosMedicos_Click(object sender, EventArgs e)
        {
            LimpiarDatosMedicos();
        }
        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                datoMed.ModificarDatoMedico(txt_CP.Text, cbxTipoSangre.SelectedItem.ToString(), cbxAsegurado.SelectedItem.ToString(), cbxAlergias.SelectedItem.ToString(), txt_desc_alergias.Text, cbxEnfer_Cronicas.SelectedItem.ToString(), txt_desc_enfermedades.Text, txt_num_emergencia.Text, txt_parentesco.Text);
                bitacora.MInsertarBitacora("Modificar", "Modificacion Dato Medico", NomUser, DateTime.Today);
                MessageBox.Show("Dato Medico actualizado con exito", "Actualización Dato Médico", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimpiarDatosMedicos();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al actualizar Dato Médico. Verifique nuevamente.", "Actualizar datos médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void cbxAlergias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxAlergias.SelectedIndex == 1)
            {
                txt_desc_alergias.Enabled = false;
            }
            else
            {
                txt_desc_alergias.Enabled = true;
            }
        }
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (txtCedula.MaskFull)
            {
                String cedula = txtCedula.Text;

                try
                {
                    DataSet dsValidar = datoMed.ConsultarDatosMedicosPorCedula(cedula);
                    if (dsValidar.Tables[0].Rows.Count > 0)
                    {
                        btn_Actualizar.Enabled = true;
                        btn_guardarDatoMedico.Enabled = false;
                    }
                    else
                    {
                        btn_Actualizar.Enabled = false;
                        btn_guardarDatoMedico.Enabled = true;
                        cbxAlergias.SelectedIndex = -1;
                        cbxTipoSangre.SelectedIndex = -1;
                        cbxAsegurado.SelectedIndex = -1;
                        txt_desc_alergias.Clear();
                        cbxEnfer_Cronicas.SelectedIndex = -1;
                        txt_desc_enfermedades.Clear();
                        txt_parentesco.Clear();
                        txt_num_emergencia.Clear();
                    }
                    // Datos medicos
                    String respuesta = datoMed.TraeDatosMedicos(cedula);
                    if (respuesta != ".........")
                    {
                        String[] separar = respuesta.Split('.');
                        /*APARTADO DE LA PRECONSULTA*/
                        txtCedulaPaciente.Text = separar[0];
                        lbl_Nombre.Text = separar[9];
                        /*FIN APARTADO DE LA PRECONSULTA*/
                        txt_CP.Text = separar[0];
                        cbxTipoSangre.Text = separar[1];
                        cbxAsegurado.Text = separar[2];
                        cbxAlergias.Text = separar[3];
                        txt_desc_alergias.Text = separar[4];
                        cbxEnfer_Cronicas.Text = separar[5];
                        txt_desc_enfermedades.Text = separar[6];
                        txt_num_emergencia.Text = separar[7];
                        txt_parentesco.Text = separar[8];
                        txt_nombreCompleto.Text = separar[9];
                      
                    }
                    else
                    {
                        String resp2 = paciente.TraePaciente(cedula);
                        String[] separar2 = resp2.Split('.');
                        txt_CP.Text = separar2[7];
                        /*APARTADO DE LA PRECONSULTA*/
                        txtCedulaPaciente.Text = separar2[7];
                        lbl_Nombre.Text = separar2[0];
                        /*FIN APARTADO DE LA PRECONSULTA*/
                        txt_nombreCompleto.Text = separar2[0];
                       
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Error al cargar los datos", "Cargar PreConsultas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Campo de búsqueda incompleto.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void txtPeso_TextChanged(object sender, EventArgs e) 
        {
            if (txtEstatura.MaskFull && !String.IsNullOrEmpty(txtPeso.Text))
            {
                double I_IMC = (Convert.ToDouble(txtPeso.Text) / Math.Pow(Convert.ToDouble(txtEstatura.Text), 2));
                lbl_IMC.Text = I_IMC.ToString();
            }
        }
        private void txtEstatura_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtEstatura.Text) && !String.IsNullOrEmpty(txtPeso.Text))
            {
                double I_IMC = (Convert.ToDouble(txtPeso.Text) / Math.Pow(Convert.ToDouble(txtEstatura.Text), 2));
                lbl_IMC.Text = I_IMC.ToString();
            }
        }
        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private void txtTemperatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=36 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }      
    }
}
