﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;

namespace CLINICA
{
    public partial class frm_consultaCitas : Form
    {
        CONEXION.ReservaCitas citas = new CONEXION.ReservaCitas();
        CONEXION.Paciente pac = new CONEXION.Paciente();
        public int id = 0;

        public frm_consultaCitas()
        {
            InitializeComponent();
        }
        public void limpiar()
        {
            txtNombre.Clear();
            txtIdentif.Clear();
            txtCedula.Clear();
            txtNomb.Clear();
            txtTel.Clear();
            txtFecha.Clear();
            txtHora.Clear();
            DtFecha.Text = String.Empty;
        }


        private void btnBuscarCita_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombre.Text) && !txtIdentif.MaskFull)
            {
                MessageBox.Show("Ingrese el N° de cédula o el nombre del paciente, por favor", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!String.IsNullOrEmpty(txtNombre.Text) || !txtIdentif.MaskFull)
            {
                DateTime fecha = Convert.ToDateTime(DtFecha.Text);
                String cedula = txtIdentif.Text, nombre = txtNombre.Text;
                String respuesta = citas.BuscarCita("Nombre", cedula, nombre, fecha);
                String[] separar = respuesta.Split('$');
                txtCedula.Text = separar[0];
                txtNomb.Text = separar[1];
                txtTel.Text = separar[2];
                txtFecha.Text = separar[3];
                txtHora.Text = separar[4];
                id = Convert.ToInt32(separar[5]);
            }
            else if (String.IsNullOrEmpty(txtNombre.Text) || txtIdentif.MaskFull)
            {
                DateTime fecha = Convert.ToDateTime(DtFecha.Text);
                String cedula = txtIdentif.Text, nombre = txtNombre.Text;
                String respuesta = citas.BuscarCita("Cedula", cedula, nombre, fecha);
                String[] separar = respuesta.Split('$');
                txtCedula.Text = separar[0];
                txtNomb.Text = separar[1];
                txtTel.Text = separar[2];
                txtFecha.Text = separar[3];
                txtHora.Text = separar[4];
                id = Convert.ToInt32(separar[5]);
            }
            else if (!String.IsNullOrEmpty(txtNombre.Text) || txtIdentif.MaskFull)
            {
                DateTime fecha = Convert.ToDateTime(DtFecha.Text);
                String cedula = txtIdentif.Text, nombre = txtNombre.Text;
                String respuesta = citas.BuscarCita("Dos", cedula, nombre, fecha);
                String[] separar = respuesta.Split('$');
                txtCedula.Text = separar[0];
                txtNomb.Text = separar[1];
                txtTel.Text = separar[2];
                txtFecha.Text = separar[3];
                txtHora.Text = separar[4];
                id = Convert.ToInt32(separar[5]);
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
          
        }

        private void btn_Confirmar_Click(object sender, EventArgs e)
        {
            String respuesta = pac.BuscarPaciente(txtCedula.Text);
            if (respuesta.Equals("Si"))
            {
                //citas.ProcesarCita(id);
                frm_preConsulta pre = new frm_preConsulta();
                pre.txtCedulaPaciente.Text = txtCedula.Text;
                //pre.Lbl_Mensaje.Text = "Paciente: " + txtNomb.Text;
                //pre.Lbl_Mensaje.Visible = true;
                pre.Show();
                this.Hide();
            }
            else
            {
                //citas.ProcesarCita(id);
                String nombre = txtNomb.Text;
                String[] separar = nombre.Split(' ');
                frm_paciente paciente = new frm_paciente();
                paciente.txtCedula.Text = txtCedula.Text;
                paciente.txtNombre.Text = separar[0];
                if (!String.IsNullOrEmpty(separar[1]))
                {
                    paciente.txtPrimerApe.Text = separar[1];
                }                
                paciente.txtCelular.Text = txtTel.Text;
                paciente.Show();
                this.Hide();
            }
        }
    }
}
