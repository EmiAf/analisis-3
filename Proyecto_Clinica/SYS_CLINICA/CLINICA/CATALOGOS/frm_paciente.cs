﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_paciente : Form
    {
        CONEXION.Paciente paciente = new CONEXION.Paciente();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        CONEXION.Rol rol = new CONEXION.Rol();
        CONEXION.Global global = new CONEXION.Global();
        private DataSet dsGrid;
        private DataSet dsRol;
        public string NomUser;
        public frm_paciente()
        {
            InitializeComponent();
        }
        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtPrimerApe.Text) || cbxEstadoCivil.SelectedItem == null || cbxGenero.SelectedItem == null || string.IsNullOrEmpty(txtCelular.Text) 
                    || dtpFechaNacimiento.Value == null)
                {
                    MessageBox.Show("Exiten campos vacios. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtpFechaNacimiento.Value > DateTime.Today)
                {
                    MessageBox.Show("Fecha de Nacimiento incorrecta. La fecha de naciento no puede ser mayor al día de hoy.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!txtCedula.MaskCompleted)
                {
                    MessageBox.Show("Complete de manera adecuada el campo de Cédula.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!txtCelular.MaskCompleted)
                {
                    MessageBox.Show("Complete de manera adecuada el campo Célular.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    paciente.MInsertarPaciente(txtCedula.Text, txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, cbxGenero.SelectedItem.ToString(), cbxEstadoCivil.SelectedItem.ToString(), dtpFechaNacimiento.Value, txtDireccion.Text, txtTelefono.Text, txtCelular.Text, txtCorreo.Text, "Si");
                    bitacora.MInsertarBitacora("Insertar", "Insercion Paciente", NomUser, DateTime.Today);
                    CargarGridPaciente();
                    Limpiar();
                    MessageBox.Show("El paciente se agregó con éxito", "Ingreso Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al registrar paciente. Verifique nuevamente.", "Ingreso Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void CargarGridPaciente()
        {
            try
            {
                dsGrid = paciente.MConsultarTodosPaciente();
                dgvPaciente.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void CargarComboActivo()
        {
            cbxActivo.Items.Add("Si");
            cbxActivo.Items.Add("No");
        }
        private void CargarComboGenero()
        {
            cbxGenero.Items.Add("Masculino");
            cbxGenero.Items.Add("Femenino");
        }
        
        private void CargarComboEstado()
        {
            cbxEstadoCivil.Items.Add("Soltero/a");
            cbxEstadoCivil.Items.Add("Casado/a");
            cbxEstadoCivil.Items.Add("Divorciado/a");
            cbxEstadoCivil.Items.Add("Viudo/a");
        }

        public void Limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            txtPrimerApe.Clear();
            txtSegundoApe.Clear();
            txtTelefono.Clear();
            txtCelular.Clear();
            txtDireccion.Clear();
            txtCorreo.Clear();
            dtpFechaNacimiento.Value = DateTime.Today;
            cb_activar.Checked = false;
            cb_activar.Visible = false;
            cbxGenero.SelectedIndex = -1;
            cbxEstadoCivil.SelectedIndex = -1;
            txtCedula.ReadOnly = false;
            lbl_estado.Text = "--";
        }
        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtPrimerApe.Text) || cbxEstadoCivil.SelectedItem == null || cbxGenero.SelectedItem == null || string.IsNullOrEmpty(txtCelular.Text) || dtpFechaNacimiento.Value == null)               
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {                    
                    if (cb_activar.Checked == true)
                    {
                        paciente.MModificarPaciente(txtCedula.Text, txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, cbxGenero.SelectedItem.ToString(), cbxEstadoCivil.SelectedItem.ToString(), dtpFechaNacimiento.Value, txtDireccion.Text, txtTelefono.Text, txtCelular.Text, txtCorreo.Text, "Si");
                        bitacora.MInsertarBitacora("Modificar", "Modificacion Paciente", NomUser, DateTime.Today);
                        CargarGridPaciente();
                        MessageBox.Show("Paciente editado con éxito", "Actualizacíon Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Limpiar();
                    }
                    else
                    {
                        paciente.MModificarPacienteAlternative(txtCedula.Text, txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, cbxGenero.SelectedItem.ToString(), cbxEstadoCivil.SelectedItem.ToString(), dtpFechaNacimiento.Value, txtDireccion.Text, txtTelefono.Text, txtCelular.Text, txtCorreo.Text);
                        bitacora.MInsertarBitacora("Modificar", "Modificacion Paciente", NomUser, DateTime.Today);
                        CargarGridPaciente();
                        MessageBox.Show("Paciente editado con éxito", "Actualizacíon Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Limpiar();
                    }  
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al editar paciente. Verifique nuevamente.", "Actualización Paciente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text))
                {
                    MessageBox.Show("Campo Cédula vacío. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    paciente.MEliminarPaciente("No", txtCedula.Text);
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Paciente", NomUser, DateTime.Today);
                    CargarGridPaciente();
                    MessageBox.Show("Paciente Inhabilitado", "Inhabilitar Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al inhabilitar paciente. Verifique nuevamente.", "Inhabilitar Paciente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void frm_paciente_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            this.inicio();
            CargarGridPaciente();            
            CargarComboActivo();
            CargarComboGenero();
            CargarComboEstado();
            cb_activar.Visible = false;
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
            this.inicio();
        }

        public void inicio()
        {
            btn_Actualizar.Enabled = false;
            btn_eliminar.Enabled = false;
            btn_Ingresar.Enabled = true;
        }

        private void dgvPaciente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0 || e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5
                || e.ColumnIndex == 6 || e.ColumnIndex == 7 || e.ColumnIndex == 8 || e.ColumnIndex == 9 || e.ColumnIndex == 10 || e.ColumnIndex == 11
                || e.ColumnIndex == 12) && e.RowIndex >= 0)
            {
                txtCedula.ReadOnly = true;
                txtCedula.Text = dgvPaciente.CurrentRow.Cells[0].Value.ToString();                          
                txtNombre.Text = dgvPaciente.CurrentRow.Cells[1].Value.ToString();           
                txtPrimerApe.Text = dgvPaciente.CurrentRow.Cells[2].Value.ToString();
                txtSegundoApe.Text = dgvPaciente.CurrentRow.Cells[3].Value.ToString();                

                string currentGenero = dgvPaciente.CurrentRow.Cells[5].Value.ToString();
                if (currentGenero.Trim() == "Masculino")
                {
                    cbxGenero.SelectedIndex = 0;
                }
                else
                {
                    cbxGenero.SelectedIndex = 1;
                }

                string currentEstadoCivil = dgvPaciente.CurrentRow.Cells[6].Value.ToString();
                if (currentEstadoCivil.Trim() == "Soltero/a")
                {
                    cbxEstadoCivil.SelectedIndex = 0;
                }
                else if (currentEstadoCivil.Trim() == "Casado/a")
                {
                    cbxEstadoCivil.SelectedIndex = 1;
                }
                else if (currentEstadoCivil.Trim() == "Divorciado/a")
                {
                    cbxEstadoCivil.SelectedIndex = 2;
                }
                else if (currentEstadoCivil.Trim() == "Viudo/a")
                {
                    cbxEstadoCivil.SelectedIndex = 3;
                }
                
                dtpFechaNacimiento.Value = DateTime.Parse(dgvPaciente.CurrentRow.Cells[7].Value.ToString());
                txtDireccion.Text = dgvPaciente.CurrentRow.Cells[8].Value.ToString();
                txtTelefono.Text = dgvPaciente.CurrentRow.Cells[9].Value.ToString();
                txtCelular.Text = dgvPaciente.CurrentRow.Cells[10].Value.ToString();
                txtCorreo.Text = dgvPaciente.CurrentRow.Cells[11].Value.ToString();
               
                string currentActivo = dgvPaciente.CurrentRow.Cells[12].Value.ToString();
                if (currentActivo.Trim() == "Si")
                {                    
                    label13.Visible = true;
                    lbl_estado.Text = "Activo";
                    lbl_estado.Visible = true;
                    cb_activar.Visible = false;
                    btn_eliminar.Enabled = true;                    
                }
                else if (currentActivo.Trim() == "No")
                {                    
                    lbl_estado.Text = "Inactivo";
                    lbl_estado.Visible = true;
                    cb_activar.Visible = true;
                    btn_eliminar.Enabled = false;
                }                
            }
            btn_Actualizar.Enabled = true;
            btn_Ingresar.Enabled = false;
        }

        private void lkLblCerrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void cbxEstadoCivil_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=20 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }
    }
}
