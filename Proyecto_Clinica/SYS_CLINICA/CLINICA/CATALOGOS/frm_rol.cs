﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class Rol : Form
    {
        
        CONEXION.Rol rol = new CONEXION.Rol();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        private DataSet dsGrid;
        private DataSet dsRol;
        public Rol()
        {
            InitializeComponent();
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
               if (string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    rol.MInsertarRol(txtNombre.Text,txtDescripcion.Text);
                    cargarGridRoles();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al insertar, por favor verificar.", "Insertar datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cargarGridRoles()
        {
            try
            {
                dsGrid = rol.MConsultarTodosRoles();
                dgvRol.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error cargar los datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDescripcion.Text) || string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    rol.MModificarRol(txtNombre.Text,txtDescripcion.Text,Int32.Parse(txtCodigo.Text));
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Rol", NomUser, DateTime.Today);
                    cargarGridRoles();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al modicar, por favor verificar.", "Modificar datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    rol.MEliminarRol(Convert.ToInt32(txtCodigo.Text));
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Rol", NomUser, DateTime.Today);
                    cargarGridRoles();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al eliminar, por favor verificar.", "Eliminar datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            txtDescripcion.Clear();
            txtCodigo.Clear();
            txtNombre.Clear();
        }

        private void dgvRol_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtCodigo.Text = dgvRol.CurrentRow.Cells[0].Value.ToString();
                txtNombre.Text = dgvRol.CurrentRow.Cells[1].Value.ToString();
                txtDescripcion.Text = dgvRol.CurrentRow.Cells[2].Value.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione una celda válida","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Rol_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            cargarGridRoles();
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void pictureHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=26 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

    }
}
