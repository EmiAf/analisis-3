﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_medicamento : Form
    {
        private DataSet dsGrid;
        CONEXION.Cl_Medicamento medic = new CONEXION.Cl_Medicamento();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;

        public frm_medicamento()
        {
            InitializeComponent();
        }
        public void Limpiar()
        {
            txtCodigo.Text = string.Empty;
            txtDescripcion.Text = string.Empty;
            txtNombre.Text = string.Empty;
        }
        public void habilitar(bool ingresar,bool actualizar)
        {
            btn_Ingresar.Enabled = ingresar;
            btn_Actualizar.Enabled = actualizar;
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
            habilitar(true, false);
        }
        private void CargaMedicamento()
        {
            try
            {
                dsGrid = medic.ConsultaTodosMedicamentos();
                dgvMedicamento.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar los datos", " Cargar medicamentos ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void frm_medicamento_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            CargaMedicamento();
        }
        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Exiten campos vacios, Por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    medic.InsertarMedicamento(txtNombre.Text, txtDescripcion.Text,"1");
                    bitacora.MInsertarBitacora("Insertar", "Insercion Medicamento", NomUser, DateTime.Today);
                    CargaMedicamento();
                    MessageBox.Show("El medicamento se guardó con éxto", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al insertar, por favor verificar.", "Insertar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    medic.EliminarMedicamento(Convert.ToInt32(txtCodigo.Text));
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Medicamento", NomUser, DateTime.Today);
                    CargaMedicamento();
                    MessageBox.Show("El medicamento se eliminó con éxito", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al eliminar, Por favor digite un código", " Eliminar datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDescripcion.Text) || string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Existen campos vacios, Por favor verificar.", " Validacion de datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    medic.ActualizaMedicamento(Int32.Parse(txtCodigo.Text), txtNombre.Text, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Medicamento", NomUser, DateTime.Today);
                    CargaMedicamento();
                    MessageBox.Show("El medicamento se modificó con éxito", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al modificar, Por favor verificar.", "Modificar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
        private void dgvMedicamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtCodigo.Text = dgvMedicamento.CurrentRow.Cells[0].Value.ToString();
                txtNombre.Text = dgvMedicamento.CurrentRow.Cells[1].Value.ToString();
                txtDescripcion.Text = dgvMedicamento.CurrentRow.Cells[2].Value.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione una celda válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.habilitar(false, true);
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=17 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }
    }
}
