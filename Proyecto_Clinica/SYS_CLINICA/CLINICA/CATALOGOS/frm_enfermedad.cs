﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_enfermedad : Form
    {
        CONEXION.Cl_Enfermedad Enfermedad = new CONEXION.Cl_Enfermedad();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        private DataSet dsGrid;
        public string NomUser;
        public frm_enfermedad()
        {
            InitializeComponent();
        }

        public void Limpiar()
        {
            txtCodigo.Text = String.Empty;
            txtNombre.Text = String.Empty;
            txtDescripcion.Text = String.Empty;
        }

        public void habilitar(bool ingresar, bool actualizar)
        {
            this.btn_Ingresar.Enabled = ingresar;
            this.btn_Actualizar.Enabled = actualizar;
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Exiten campos vacios, Por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Enfermedad.InsertarEnfermedad(txtNombre.Text, txtDescripcion.Text,"1");
                    bitacora.MInsertarBitacora("Insertar", "Insercion Enfermedad", NomUser, DateTime.Today);
                    cargarEnfermedades();
                    MessageBox.Show("La enfermedad se guardó con éxito.", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar, por favor verificar." + ex.ToString(), "Guardar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cargarEnfermedades()
        {
            try
            {
                dsGrid = Enfermedad.ConsultaTodasEnferm();
                dgvEnfermedad.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error cargar los datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void frm_enfermedad_Load(object sender, EventArgs e)
        {
            cargarEnfermedades();
            this.NomUser = Session.UserSesion;
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
            habilitar(true, false);
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Enfermedad.EliminarEnfermedad(Convert.ToInt32(txtCodigo.Text));
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Enfermedad", NomUser, DateTime.Today);
                    cargarEnfermedades();
                    MessageBox.Show("Se ha eliminado una enfermedad", "Sistema Clinica", MessageBoxButtons.OK , MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al eliminar, Por favor digite un código", " Eliminar datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Exiten campos vacios, Por favor verificar.", " Validacion de datos ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Enfermedad.ActualizaEnfermedad(Int32.Parse(txtCodigo.Text),txtNombre.Text, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Enfermedad", NomUser, DateTime.Today);
                    cargarEnfermedades();
                    MessageBox.Show("La enfermedad se modificó con éxito.", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    habilitar(true, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al modificar, Por favor verificar.", "Modificar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
        private void dgvEnfermedad_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtCodigo.Text = dgvEnfermedad.CurrentRow.Cells[0].Value.ToString();
                txtNombre.Text = dgvEnfermedad.CurrentRow.Cells[1].Value.ToString();
                txtDescripcion.Text = dgvEnfermedad.CurrentRow.Cells[2].Value.ToString();
                this.habilitar(false, true);
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione una celda válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=14 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

    }
}
