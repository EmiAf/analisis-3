﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_consulta : Form
    {
        CONEXION.PreConsulta pre = new CONEXION.PreConsulta();
        CONEXION.Paciente pac = new CONEXION.Paciente();
        CONEXION.ConsultaMedica cm = new CONEXION.ConsultaMedica();
        CONEXION.Cl_Examenes exa = new CONEXION.Cl_Examenes();
        CONEXION.Cl_Medicamento medicamentos = new CONEXION.Cl_Medicamento();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        public void ExamenesTemporales()
        {
            DataSet dse = new DataSet();
            dse = exa.TraeExamenTemporales();
            DGV_ExaTemp.DataSource = dse.Tables[0];
        }
        public frm_consulta()
        {
            InitializeComponent();
            ProxId();
            this.NomUser = Session.UserSesion;
        }
        public void ProxId()
        {
            lblNumConsulta.Text = cm.ProximoId().ToString();
        }
        private void frm_consulta_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
        }
        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("No se guardaran los cambios, desea salir?", "Consulta.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                medicamentos.EliminarTemporales();
                this.Close();
            }

        }
        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            tm_consulta.Start();
        }
        private void btnAgregarMedicamento_Click(object sender, EventArgs e)
        {
            if (txtCedula.MaskFull)
            {
                CLINICA.CATALOGOS.frm_AgregarMedicamento medicamento = new CATALOGOS.frm_AgregarMedicamento();
                medicamento.txtCedula.Text = txtCedula.Text;
                medicamento.Txt_Consulta.Text = lblNumConsulta.Text;
                medicamento.Show();
            }
            else
            {
                MessageBox.Show("Digite el número de cédula del paciente", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnAgregarExamen_Click(object sender, EventArgs e)
        {
            if (txtCedula.MaskFull)
            {
                frm_AgregarExamen examen = new frm_AgregarExamen();
                examen.txtCedula.Text = txtCedula.Text;
                examen.Txt_Consulta.Text = lblNumConsulta.Text;
                examen.Show();
            }
            else
            {
                MessageBox.Show("Digite el número de cédula del paciente", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        private void Lbl_Exámenes_Click(object sender, EventArgs e)
        {
            ExamenesTemporales();
        }
        private void btnGuardarConsulta_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtSintomas.Text) || String.IsNullOrEmpty(txtDiagnostico.Text) || !txtCedula.MaskFull)
                MessageBox.Show("Debe de ingresar los síntomas y el diagnóstico", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (DGV_ExaTemp.Rows.Count == 0)
                {
                    // SIN EXAMENES
                    try
                    {
                        String sintomas = txtSintomas.Text, diagnostico = txtDiagnostico.Text, cedula = txtCedula.Text, fecha = DateTime.Now.ToShortDateString();
                        DateTime fechaConsulta = Convert.ToDateTime(fecha);
                        cm.InsertarConsulta(cedula, fechaConsulta, sintomas, diagnostico);
                        bitacora.MInsertarBitacora("Insertar", "Insercion Consulta", NomUser, DateTime.Today);
                        MessageBox.Show("La consulta se guardó con exito", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Limpiar();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("La consulta no se pudo guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    // CON EXAMENES - PASARLOS
                    try
                    {
                        String sintomas = txtSintomas.Text, diagnostico = txtDiagnostico.Text, cedula = txtCedula.Text, fecha = DateTime.Now.ToShortDateString();
                        DateTime fechaConsulta = Convert.ToDateTime(fecha);
                        cm.InsertarConsulta(cedula, fechaConsulta, sintomas, diagnostico);
                        bitacora.MInsertarBitacora("Insertar", "Insercion Consulta", NomUser, DateTime.Today);
                        exa.AgregarExamenXPaciente();
                        bitacora.MInsertarBitacora("Insertar", "Insercion Examen-Paciente", NomUser, DateTime.Today);
                        medicamentos.AgregarMedicamentoXPaciente();
                        bitacora.MInsertarBitacora("Insertar", "Insercion Medicamento-Paciente", NomUser, DateTime.Today);
                        MessageBox.Show("La consulta se guardó con exito", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpiar();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("La consulta no se pudo guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void btn_cargar_Click(object sender, EventArgs e)
        {
            if (txtCedula.MaskFull)
            {
                medicamentos.EliminarTemporales();
                String cedula = txtCedula.Text;
                try
                {
                    // PreConsulta
                    DataSet ds = new DataSet();
                    ds = pre.TraePreConsultas(cedula);
                    DGV_Preconsulta.DataSource = ds.Tables[0];

                    // Datos personales
                    String respuesta = pac.TraePaciente(cedula);
                    if (respuesta.Equals("......."))
                    {
                        MessageBox.Show("No existe ningún paciente con ese número de cédula", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtCedula.Clear();
                        txtCedula.Focus();
                    }
                    else
                    {
                        String[] separar = respuesta.Split('.');
                        txtNombre.Text = separar[0];
                        txtTipoSangre.Text = separar[1];
                        txtGenero.Text = separar[2];
                        txtEstadoCivil.Text = separar[3];
                        txtFechaNac.Text = separar[4];
                        txtDireccion.Text = separar[5];
                        txtTelefono.Text = separar[6];

                        // Examenes
                        DataSet dse = new DataSet();
                        dse = exa.TraeExamenXPaciente(cedula);
                        DGV_Examenes.DataSource = dse.Tables[0];
                        MessageBox.Show("Información del paciente cargada.","Sistema Clínica Médica");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error al cargar los datos", "Cargar PreConsultas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void tm_consulta_Tick(object sender, EventArgs e)
        {
            DataSet dsTemporal = exa.TraeExamenTemporal(txtCedula.Text);
            DGV_ExaTemp.DataSource = dsTemporal.Tables[0];

            DataSet dsTemporalMedi = medicamentos.TraerMedicamentoTemporal(txtCedula.Text);
            DGV_MediTemp.DataSource = dsTemporalMedi.Tables[0];
        }
        private void Limpiar()
        {
            txtCedula.Clear();
            txtDiagnostico.Clear();
            txtDireccion.Clear();
            txtEstadoCivil.Clear();
            txtFechaNac.Clear();
            txtGenero.Clear();
            txtNombre.Clear();
            txtSintomas.Clear();
            txtTelefono.Clear();
            txtTipoSangre.Clear();
            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtSintomas.Text = string.Empty;
            txtDiagnostico.Text = string.Empty;
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=39 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }
    }
}
