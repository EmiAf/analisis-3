﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA.CATALOGOS
{
    public partial class frm_AgregarMedicamento : Form 
    {
        CONEXION.Cl_Medicamento medicamento = new CONEXION.Cl_Medicamento();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        public frm_AgregarMedicamento()
        {
            InitializeComponent();
            cargarMedicamentos();
            this.NomUser = Session.UserSesion;
        }

        private void cargarMedicamentos()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = medicamento.ConsultaTodosMedicamentos();
                CB_Medicamento.DataSource = ds.Tables[0];
                CB_Medicamento.DisplayMember = "Nombre";
                CB_Medicamento.ValueMember = "Id_Medicamento";
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar los datos", " Cargar Medicamentos ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtDescripcion.Text))
            {
                MessageBox.Show("Seleccione un medicamento", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int idMedicamento = Convert.ToInt32(CB_Medicamento.SelectedValue), consulta = int.Parse(Txt_Consulta.Text);
                DateTime fecha = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                String descripcion = txtDescripcion.Text, cedula = txtCedula.Text;
                medicamento.AgregarMedicamentoTemporal(idMedicamento, cedula, descripcion, fecha, consulta);
                bitacora.MInsertarBitacora("Insertar", "Insercion Medicamento", NomUser, DateTime.Today);
                MessageBox.Show("El medicamento se agregó con exito.", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void CB_Medicamento_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int valor = int.Parse(CB_Medicamento.SelectedValue.ToString());
            txtDescripcion.Text = medicamento.DescripcionMedicamento(valor);
        }
    }
}
