﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CLINICA
{
    public partial class frm_AgregarExamen : Form
    {
        CONEXION.Cl_Examenes Examen = new CONEXION.Cl_Examenes();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        public static Boolean paso = false;

        public frm_AgregarExamen()
        {
            InitializeComponent();
            cargarExamenes();
            this.NomUser = Session.UserSesion;
        }

        private void cargarExamenes()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = Examen.ConsultaTodosExamen();
                CB_Examenes.DataSource = ds.Tables[0];
                CB_Examenes.DisplayMember = "Nombre";
                CB_Examenes.ValueMember = "Id_Examen";
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar los datos", " Cargar Examenes ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtDescripcion.Text))
            {
                MessageBox.Show("Seleccione un exámen", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int idExamen = Convert.ToInt32(CB_Examenes.SelectedValue), consulta = int.Parse(Txt_Consulta.Text);
                DateTime fecha = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                String descripcion = txtDescripcion.Text, cedula = txtCedula.Text;
                Examen.AgregarExamenTemporal(idExamen, cedula, descripcion,fecha, consulta);
                bitacora.MInsertarBitacora("Insertar", "Insercion Examen", NomUser, DateTime.Today);
                MessageBox.Show("El exámen se agregó con exito.", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            frm_consulta aa = new frm_consulta();
            
        }

        private void CB_Examenes_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int valor = int.Parse(CB_Examenes.SelectedValue.ToString());
            txtDescripcion.Text = Examen.DescripcionExamen(valor);
        }

        private void Txt_Consulta_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
