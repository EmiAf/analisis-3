﻿namespace CLINICA
{
    partial class frm_preConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_preConsulta));
            this.label9 = new System.Windows.Forms.Label();
            this.lkLblMenu = new System.Windows.Forms.LinkLabel();
            this.txtCedula = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txt_num_emergencia = new System.Windows.Forms.MaskedTextBox();
            this.txt_parentesco = new System.Windows.Forms.MaskedTextBox();
            this.txt_CP = new System.Windows.Forms.Label();
            this.txt_nombreCompleto = new System.Windows.Forms.Label();
            this.btn_Actualizar = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_desc_enfermedades = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbxEnfer_Cronicas = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_desc_alergias = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbxAlergias = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbxAsegurado = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxTipoSangre = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_guardarDatoMedico = new System.Windows.Forms.Button();
            this.btn_limpiar_datosMedicos = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtEstatura = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPresionArterial = new System.Windows.Forms.MaskedTextBox();
            this.lbl_IMC = new System.Windows.Forms.Label();
            this.lbl_Nombre = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnGuardarPreconsulta = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.txtDiagnostico = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTemperatura = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtCedulaPaciente = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictHelp = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(345, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 20);
            this.label9.TabIndex = 63;
            this.label9.Text = "Pre Consulta";
            // 
            // lkLblMenu
            // 
            this.lkLblMenu.AutoSize = true;
            this.lkLblMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkLblMenu.Location = new System.Drawing.Point(821, 567);
            this.lkLblMenu.Name = "lkLblMenu";
            this.lkLblMenu.Size = new System.Drawing.Size(50, 18);
            this.lkLblMenu.TabIndex = 70;
            this.lkLblMenu.TabStop = true;
            this.lkLblMenu.Text = "Cerrar";
            this.lkLblMenu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkLblMenu_LinkClicked);
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(194, 66);
            this.txtCedula.Mask = "0-0000-0000";
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(128, 22);
            this.txtCedula.TabIndex = 73;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 69);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 16);
            this.label7.TabIndex = 72;
            this.label7.Text = "Cédula del paciente:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(30, 117);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(866, 447);
            this.tabControl1.TabIndex = 72;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Lavender;
            this.tabPage2.Controls.Add(this.txt_num_emergencia);
            this.tabPage2.Controls.Add(this.txt_parentesco);
            this.tabPage2.Controls.Add(this.txt_CP);
            this.tabPage2.Controls.Add(this.txt_nombreCompleto);
            this.tabPage2.Controls.Add(this.btn_Actualizar);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.txt_desc_enfermedades);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.cbxEnfer_Cronicas);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txt_desc_alergias);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.cbxAlergias);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.cbxAsegurado);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.cbxTipoSangre);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.btn_guardarDatoMedico);
            this.tabPage2.Controls.Add(this.btn_limpiar_datosMedicos);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(858, 418);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Datos Medicos";
            // 
            // txt_num_emergencia
            // 
            this.txt_num_emergencia.Location = new System.Drawing.Point(159, 269);
            this.txt_num_emergencia.Mask = "0000-0000";
            this.txt_num_emergencia.Name = "txt_num_emergencia";
            this.txt_num_emergencia.Size = new System.Drawing.Size(110, 22);
            this.txt_num_emergencia.TabIndex = 60;
            // 
            // txt_parentesco
            // 
            this.txt_parentesco.Location = new System.Drawing.Point(483, 283);
            this.txt_parentesco.Name = "txt_parentesco";
            this.txt_parentesco.Size = new System.Drawing.Size(205, 22);
            this.txt_parentesco.TabIndex = 59;
            // 
            // txt_CP
            // 
            this.txt_CP.AutoSize = true;
            this.txt_CP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_CP.Location = new System.Drawing.Point(599, 13);
            this.txt_CP.Name = "txt_CP";
            this.txt_CP.Size = new System.Drawing.Size(18, 16);
            this.txt_CP.TabIndex = 58;
            this.txt_CP.Text = "--";
            // 
            // txt_nombreCompleto
            // 
            this.txt_nombreCompleto.AutoSize = true;
            this.txt_nombreCompleto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nombreCompleto.Location = new System.Drawing.Point(155, 13);
            this.txt_nombreCompleto.Name = "txt_nombreCompleto";
            this.txt_nombreCompleto.Size = new System.Drawing.Size(18, 16);
            this.txt_nombreCompleto.TabIndex = 57;
            this.txt_nombreCompleto.Text = "--";
            // 
            // btn_Actualizar
            // 
            this.btn_Actualizar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Actualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_Actualizar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_Actualizar.Image = global::CLINICA.Properties.Resources.actualizar_restaure_agt_icono_7628_32;
            this.btn_Actualizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Actualizar.Location = new System.Drawing.Point(356, 348);
            this.btn_Actualizar.Name = "btn_Actualizar";
            this.btn_Actualizar.Size = new System.Drawing.Size(110, 60);
            this.btn_Actualizar.TabIndex = 56;
            this.btn_Actualizar.Text = "Actualizar";
            this.btn_Actualizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Actualizar.UseVisualStyleBackColor = true;
            this.btn_Actualizar.Click += new System.EventHandler(this.btn_Actualizar_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(458, 13);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 16);
            this.label19.TabIndex = 54;
            this.label19.Text = "Cédula paciente:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 269);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(110, 32);
            this.label18.TabIndex = 50;
            this.label18.Text = "Número en caso \r\nde emegencia:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(300, 269);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 32);
            this.label17.TabIndex = 48;
            this.label17.Text = "En caso de \r\nemergencia llamar a:";
            // 
            // txt_desc_enfermedades
            // 
            this.txt_desc_enfermedades.Location = new System.Drawing.Point(459, 199);
            this.txt_desc_enfermedades.Margin = new System.Windows.Forms.Padding(4);
            this.txt_desc_enfermedades.Multiline = true;
            this.txt_desc_enfermedades.Name = "txt_desc_enfermedades";
            this.txt_desc_enfermedades.Size = new System.Drawing.Size(379, 66);
            this.txt_desc_enfermedades.TabIndex = 47;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(303, 181);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 32);
            this.label16.TabIndex = 46;
            this.label16.Text = "Ingrese \r\nenfermedades:";
            // 
            // cbxEnfer_Cronicas
            // 
            this.cbxEnfer_Cronicas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxEnfer_Cronicas.FormattingEnabled = true;
            this.cbxEnfer_Cronicas.Location = new System.Drawing.Point(159, 182);
            this.cbxEnfer_Cronicas.Name = "cbxEnfer_Cronicas";
            this.cbxEnfer_Cronicas.Size = new System.Drawing.Size(65, 24);
            this.cbxEnfer_Cronicas.TabIndex = 44;
            this.cbxEnfer_Cronicas.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 190);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 32);
            this.label15.TabIndex = 45;
            this.label15.Text = "¿Enfermedades \r\nCronicas?";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // txt_desc_alergias
            // 
            this.txt_desc_alergias.Location = new System.Drawing.Point(459, 109);
            this.txt_desc_alergias.Margin = new System.Windows.Forms.Padding(4);
            this.txt_desc_alergias.Multiline = true;
            this.txt_desc_alergias.Name = "txt_desc_alergias";
            this.txt_desc_alergias.Size = new System.Drawing.Size(379, 68);
            this.txt_desc_alergias.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(303, 109);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 16);
            this.label14.TabIndex = 42;
            this.label14.Text = "Ingrese alergías:";
            // 
            // cbxAlergias
            // 
            this.cbxAlergias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAlergias.FormattingEnabled = true;
            this.cbxAlergias.Location = new System.Drawing.Point(159, 121);
            this.cbxAlergias.Name = "cbxAlergias";
            this.cbxAlergias.Size = new System.Drawing.Size(65, 24);
            this.cbxAlergias.TabIndex = 40;
            this.cbxAlergias.SelectedIndexChanged += new System.EventHandler(this.cbxAlergias_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 16);
            this.label12.TabIndex = 41;
            this.label12.Text = "¿Tiene alergias?";
            // 
            // cbxAsegurado
            // 
            this.cbxAsegurado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAsegurado.FormattingEnabled = true;
            this.cbxAsegurado.Location = new System.Drawing.Point(459, 53);
            this.cbxAsegurado.Name = "cbxAsegurado";
            this.cbxAsegurado.Size = new System.Drawing.Size(60, 24);
            this.cbxAsegurado.TabIndex = 38;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(303, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 16);
            this.label13.TabIndex = 39;
            this.label13.Text = "¿Es Asegurado?";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 16);
            this.label11.TabIndex = 31;
            this.label11.Text = "Tipo Sangre:";
            // 
            // cbxTipoSangre
            // 
            this.cbxTipoSangre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoSangre.FormattingEnabled = true;
            this.cbxTipoSangre.Location = new System.Drawing.Point(159, 53);
            this.cbxTipoSangre.Name = "cbxTipoSangre";
            this.cbxTipoSangre.Size = new System.Drawing.Size(65, 24);
            this.cbxTipoSangre.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 13);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 13;
            this.label10.Text = "Nombre paciente:";
            // 
            // btn_guardarDatoMedico
            // 
            this.btn_guardarDatoMedico.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_guardarDatoMedico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_guardarDatoMedico.Image = global::CLINICA.Properties.Resources.save;
            this.btn_guardarDatoMedico.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_guardarDatoMedico.Location = new System.Drawing.Point(261, 348);
            this.btn_guardarDatoMedico.Margin = new System.Windows.Forms.Padding(4);
            this.btn_guardarDatoMedico.Name = "btn_guardarDatoMedico";
            this.btn_guardarDatoMedico.Size = new System.Drawing.Size(90, 60);
            this.btn_guardarDatoMedico.TabIndex = 53;
            this.btn_guardarDatoMedico.Text = "Guardar";
            this.btn_guardarDatoMedico.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_guardarDatoMedico.UseVisualStyleBackColor = true;
            this.btn_guardarDatoMedico.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_limpiar_datosMedicos
            // 
            this.btn_limpiar_datosMedicos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_limpiar_datosMedicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_limpiar_datosMedicos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_limpiar_datosMedicos.Image = global::CLINICA.Properties.Resources.cambio_de_cepillo_de_escoba_de_barrer_claro_icono_5768_32;
            this.btn_limpiar_datosMedicos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_limpiar_datosMedicos.Location = new System.Drawing.Point(470, 348);
            this.btn_limpiar_datosMedicos.Name = "btn_limpiar_datosMedicos";
            this.btn_limpiar_datosMedicos.Size = new System.Drawing.Size(90, 60);
            this.btn_limpiar_datosMedicos.TabIndex = 52;
            this.btn_limpiar_datosMedicos.Text = "Limpiar";
            this.btn_limpiar_datosMedicos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_limpiar_datosMedicos.UseVisualStyleBackColor = true;
            this.btn_limpiar_datosMedicos.Click += new System.EventHandler(this.btn_limpiar_datosMedicos_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Lavender;
            this.tabPage1.Controls.Add(this.txtEstatura);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtPresionArterial);
            this.tabPage1.Controls.Add(this.lbl_IMC);
            this.tabPage1.Controls.Add(this.lbl_Nombre);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.btnGuardarPreconsulta);
            this.tabPage1.Controls.Add(this.btnLimpiar);
            this.tabPage1.Controls.Add(this.txtDiagnostico);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtTemperatura);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtPeso);
            this.tabPage1.Controls.Add(this.txtCedulaPaciente);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(858, 418);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Datos Preconsulta";
            // 
            // txtEstatura
            // 
            this.txtEstatura.Location = new System.Drawing.Point(183, 116);
            this.txtEstatura.Mask = "0.00";
            this.txtEstatura.Name = "txtEstatura";
            this.txtEstatura.Size = new System.Drawing.Size(129, 22);
            this.txtEstatura.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 32;
            this.label3.Text = "Estatura:";
            // 
            // txtPresionArterial
            // 
            this.txtPresionArterial.Location = new System.Drawing.Point(538, 116);
            this.txtPresionArterial.Mask = "000-000";
            this.txtPresionArterial.Name = "txtPresionArterial";
            this.txtPresionArterial.Size = new System.Drawing.Size(101, 22);
            this.txtPresionArterial.TabIndex = 31;
            // 
            // lbl_IMC
            // 
            this.lbl_IMC.AutoSize = true;
            this.lbl_IMC.Location = new System.Drawing.Point(179, 81);
            this.lbl_IMC.Name = "lbl_IMC";
            this.lbl_IMC.Size = new System.Drawing.Size(16, 16);
            this.lbl_IMC.TabIndex = 29;
            this.lbl_IMC.Text = "--";
            // 
            // lbl_Nombre
            // 
            this.lbl_Nombre.AutoSize = true;
            this.lbl_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Nombre.Location = new System.Drawing.Point(555, 35);
            this.lbl_Nombre.Name = "lbl_Nombre";
            this.lbl_Nombre.Size = new System.Drawing.Size(18, 16);
            this.lbl_Nombre.TabIndex = 28;
            this.lbl_Nombre.Text = "--";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(398, 35);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 16);
            this.label20.TabIndex = 27;
            this.label20.Text = "Nombre paciente:";
            // 
            // btnGuardarPreconsulta
            // 
            this.btnGuardarPreconsulta.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardarPreconsulta.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnGuardarPreconsulta.Image = global::CLINICA.Properties.Resources.save;
            this.btnGuardarPreconsulta.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGuardarPreconsulta.Location = new System.Drawing.Point(142, 338);
            this.btnGuardarPreconsulta.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardarPreconsulta.Name = "btnGuardarPreconsulta";
            this.btnGuardarPreconsulta.Size = new System.Drawing.Size(104, 60);
            this.btnGuardarPreconsulta.TabIndex = 25;
            this.btnGuardarPreconsulta.Text = "Guardar";
            this.btnGuardarPreconsulta.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGuardarPreconsulta.UseVisualStyleBackColor = true;
            this.btnGuardarPreconsulta.Click += new System.EventHandler(this.btnGuardarPreconsulta_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLimpiar.Image = global::CLINICA.Properties.Resources.cambio_de_cepillo_de_escoba_de_barrer_claro_icono_5768_32;
            this.btnLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLimpiar.Location = new System.Drawing.Point(470, 338);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(100, 60);
            this.btnLimpiar.TabIndex = 26;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // txtDiagnostico
            // 
            this.txtDiagnostico.Location = new System.Drawing.Point(214, 220);
            this.txtDiagnostico.Margin = new System.Windows.Forms.Padding(4);
            this.txtDiagnostico.Multiline = true;
            this.txtDiagnostico.Name = "txtDiagnostico";
            this.txtDiagnostico.Size = new System.Drawing.Size(425, 100);
            this.txtDiagnostico.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(103, 223);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Diagnóstico:";
            // 
            // txtTemperatura
            // 
            this.txtTemperatura.Location = new System.Drawing.Point(538, 162);
            this.txtTemperatura.Margin = new System.Windows.Forms.Padding(4);
            this.txtTemperatura.Name = "txtTemperatura";
            this.txtTemperatura.Size = new System.Drawing.Size(101, 22);
            this.txtTemperatura.TabIndex = 21;
            this.txtTemperatura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTemperatura_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(408, 165);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "Temperatura:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(398, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "Presión arterial:";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(183, 162);
            this.txtPeso.Margin = new System.Windows.Forms.Padding(4);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(132, 22);
            this.txtPeso.TabIndex = 13;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // txtCedulaPaciente
            // 
            this.txtCedulaPaciente.Location = new System.Drawing.Point(217, 32);
            this.txtCedulaPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.txtCedulaPaciente.Name = "txtCedulaPaciente";
            this.txtCedulaPaciente.ReadOnly = true;
            this.txtCedulaPaciente.Size = new System.Drawing.Size(132, 22);
            this.txtCedulaPaciente.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 18;
            this.label4.Text = "IMC:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Cédula del paciente:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 162);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Peso:";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_buscar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_buscar.Location = new System.Drawing.Point(328, 66);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(87, 25);
            this.btn_buscar.TabIndex = 74;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CLINICA.Properties.Resources.datosPreConsulta;
            this.pictureBox1.Location = new System.Drawing.Point(728, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictHelp
            // 
            this.pictHelp.Image = ((System.Drawing.Image)(resources.GetObject("pictHelp.Image")));
            this.pictHelp.Location = new System.Drawing.Point(857, 1);
            this.pictHelp.Margin = new System.Windows.Forms.Padding(4);
            this.pictHelp.Name = "pictHelp";
            this.pictHelp.Size = new System.Drawing.Size(80, 40);
            this.pictHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictHelp.TabIndex = 75;
            this.pictHelp.TabStop = false;
            this.pictHelp.Click += new System.EventHandler(this.pictHelp_Click);
            // 
            // frm_preConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(950, 600);
            this.Controls.Add(this.pictHelp);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtCedula);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lkLblMenu);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frm_preConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_preConsulta_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHelp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.LinkLabel lkLblMenu;
        private System.Windows.Forms.MaskedTextBox txtCedula;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnGuardarPreconsulta;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.TextBox txtDiagnostico;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTemperatura;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPeso;
        public System.Windows.Forms.TextBox txtCedulaPaciente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxEnfer_Cronicas;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_desc_alergias;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbxAlergias;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxAsegurado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_desc_enfermedades;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btn_limpiar_datosMedicos;
        private System.Windows.Forms.Button btn_guardarDatoMedico;
        private System.Windows.Forms.ComboBox cbxTipoSangre;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btn_Actualizar;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label lbl_Nombre;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbl_IMC;
        private System.Windows.Forms.Label txt_nombreCompleto;
        private System.Windows.Forms.Label txt_CP;
        private System.Windows.Forms.MaskedTextBox txtPresionArterial;
        private System.Windows.Forms.MaskedTextBox txt_parentesco;
        private System.Windows.Forms.MaskedTextBox txt_num_emergencia;
        private System.Windows.Forms.MaskedTextBox txtEstatura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictHelp;
    }
}