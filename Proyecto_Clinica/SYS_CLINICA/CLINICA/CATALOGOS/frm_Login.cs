﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;

namespace CLINICA
{
    public partial class frm_Login : Form
    {
        CONEXION.Empleado empleado = new CONEXION.Empleado();
        CONEXION.Global global = new CONEXION.Global();
        public string NombreUsuario;
        public frm_Login()
        {
            InitializeComponent();            
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txtUsuario.Text) || String.IsNullOrEmpty(txtPasswd.Text))
            {
                MessageBox.Show("No se permiten datos en blanco", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                empleado.Usuario = txtUsuario.Text;
                empleado.Contrasenna = txtPasswd.Text;
                //global.globakl = empleado.Usuario;
                Session.UserSesion = txtUsuario.Text;
                String trae = empleado.Buscar();
                String[] separar = trae.Split('.');
                
                String estatus = separar[0];
                String rol = separar[1];
                String activo = separar[2];
                String nom = separar[3];
                String ape = separar[4];

                if (estatus == "True" && activo.Trim() == "A")
                {
                    MessageBox.Show("Bienvenid@ "+nom +" "+ape, "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    ((frm_principal)this.MdiParent).catalogosToolStripMenuItem.Visible = true;
                    ((frm_principal)this.MdiParent).reportesToolStripMenuItem.Visible = true;
                    ((frm_principal)this.MdiParent).citasToolStripMenuItem.Visible = true;
                    ((frm_principal)this.MdiParent).consultaMédicaToolStripMenuItem.Visible = true;
                    ((frm_principal)this.MdiParent).cerrasSesionToolStripMenuItem.Enabled = true;
                    ((frm_principal)this.MdiParent).iniciarSesionToolStripMenuItem.Enabled = false;


                    if (rol == "1")//Admin
                    {
                        
                    }
                    else if (rol == "2")//Medico
                    {

                    }
                    else if (rol == "3") //Secretaria
                    {
                        ((frm_principal)this.MdiParent).citasToolStripMenuItem.Visible = true;
                        ((frm_principal)this.MdiParent).consultaMédicaToolStripMenuItem.Visible = false;
                        ((frm_principal)this.MdiParent).catalogosToolStripMenuItem.Visible = false;                        
                    }
                    else if (rol == "4")//Enfermera
                    {
                        ((frm_principal)this.MdiParent).catalogosToolStripMenuItem.Visible = true;
                        ((frm_principal)this.MdiParent).citasToolStripMenuItem.Visible = true;
                    }
                    this.Close();
                }
                else if (estatus == "True" && activo.Trim() == "I")
                {
                    MessageBox.Show("Usuario Inactivo. Contacte al administrador para activar el usuario.", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Limpiar();
                }
                else
                {
                    MessageBox.Show("Usuario o Contraseña incorrectos", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Limpiar();
                }                
            }
        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void Limpiar()
        {
            txtUsuario.Clear();
            txtPasswd.Clear();
        }
    }
}
