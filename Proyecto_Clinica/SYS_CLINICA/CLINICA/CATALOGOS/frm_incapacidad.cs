﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace CLINICA.CATALOGOS
{
    public partial class frm_incapacidad : Form
    {
        CONEXION.Incapacidades incapacidad = new CONEXION.Incapacidades();
        CONEXION.Paciente paciente = new CONEXION.Paciente();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        private DataSet dsTipoIncapacidad;
        private DataSet dsGrid;
        public int Codigo;

        public frm_incapacidad()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(lbl_Nombre.Text) || dtp_FecEmision.Value == null || dtp_FecInicio.Value == null || dtp_FecFinal.Value == null || cbxTipoIncapacidad.SelectedItem == null || string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtp_FecEmision.Value < DateTime.Today)
                {
                    MessageBox.Show("Fecha de Emisión incorrecta. La fecha de Emisión no puede ser anterior al día de hoy.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtp_FecInicio.Value < DateTime.Today)
                {
                    MessageBox.Show("Fecha de Inicio incorrecta. La fecha de Inicio no puede ser anterior al día de hoy.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtp_FecFinal.Value < DateTime.Today)
                {
                    MessageBox.Show("Fecha de Fin incorrecta. La fecha de Fin no puede ser anterior al día de hoy.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtp_FecInicio.Value > dtp_FecFinal.Value)
                {
                    MessageBox.Show("Fecha de Inicio no puede ser mayor a la fecha Final.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtp_FecEmision.Value > dtp_FecInicio.Value || dtp_FecEmision.Value > dtp_FecFinal.Value)
                {
                    MessageBox.Show("Fecha de Inicio no puede ser mayor a la fecha Final.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    incapacidad.MInsertarIncapacidad(Convert.ToInt32(cbxTipoIncapacidad.SelectedValue), txtCedula.Text, dtp_FecEmision.Value, dtp_FecInicio.Value, dtp_FecFinal.Value, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Insertar", "Insercion Incapacidad", NomUser, DateTime.Today);
                    MessageBox.Show("Incapacidad ingresada con éxito", "Ingreso Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Limpiar();
                    lbl_Enca_Dias.Visible = false;
                    lbl_Dias_Incapacidad.Visible = false;
                    this.CargarGridIncapacidades();
                }                
            }
            catch (Exception)
            {
                MessageBox.Show("Error al registrar Incapacidad. Verifique nuevamente.", "Ingreso Incapacidades", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargarComboTipoIncapacidades()
        {
            dsTipoIncapacidad = incapacidad.MCargarTiposIncapacidades();
            cbxTipoIncapacidad.DataSource = dsTipoIncapacidad.Tables[0];
            cbxTipoIncapacidad.DisplayMember = "Nombre_Incapacidad";
            cbxTipoIncapacidad.ValueMember = "Id_Tipo_Incapacidad";
        }

        private void CargarGridIncapacidades()
        {
            try
            {
                dsGrid = incapacidad.MConsultarTodosIncapacidades();
                dgv_Incapacidades.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void Limpiar()
        {
            txtCedula.Text = "--";
            lbl_Nombre.Text = "--";            
            dtp_FecEmision.Value = DateTime.Today;
            dtp_FecInicio.Value = DateTime.Today;
            dtp_FecFinal.Value = DateTime.Today;
            cbxTipoIncapacidad.SelectedIndex = -1;
            txtDescripcion.Clear();                        
        }

        private void frm_incapacidad_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            CargarComboTipoIncapacidades();
            CargarGridIncapacidades();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (txtCedBusqueda.MaskFull)
            {
                String cedula = txtCedBusqueda.Text;
                try
                {                    
                     String resp2 = paciente.TraePaciente(cedula);
                     String[] separar2 = resp2.Split('.');
                     txtCedula.Text = separar2[7];                        
                     lbl_Nombre.Text = separar2[0];                        
                     btn_Actualizar.Enabled = false;
                     btn_Ingresar.Enabled = true;                 
                }
                catch (Exception)
                {
                    MessageBox.Show("Error al cargar los datos", "Cargar PreConsultas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Campo de búsqueda incompleto.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dgv_Incapacidades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0 || e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5
                || e.ColumnIndex == 6) && e.RowIndex >= 0)
            {
                try
                {
                    Codigo = 0; //Limpiando la variable codigo
                    txtCedula.Text = dgv_Incapacidades.CurrentRow.Cells[0].Value.ToString();
                    lbl_Nombre.Text = dgv_Incapacidades.CurrentRow.Cells[1].Value.ToString();
                    cbxTipoIncapacidad.SelectedValue = dgv_Incapacidades.CurrentRow.Cells[2].Value.ToString();
                    dtp_FecEmision.Value = DateTime.Parse(dgv_Incapacidades.CurrentRow.Cells[3].Value.ToString());                    
                    dtp_FecInicio.Value = DateTime.Parse(dgv_Incapacidades.CurrentRow.Cells[4].Value.ToString());
                    dtp_FecFinal.Value = DateTime.Parse(dgv_Incapacidades.CurrentRow.Cells[5].Value.ToString());
                    txtDescripcion.Text = dgv_Incapacidades.CurrentRow.Cells[6].Value.ToString();
                    Codigo = Convert.ToInt32(dgv_Incapacidades.CurrentRow.Cells[7].Value.ToString());
                    lbl_Dias_Incapacidad.Text = dgv_Incapacidades.CurrentRow.Cells[8].Value.ToString();
                    lbl_Enca_Dias.Visible = true;
                    lbl_Dias_Incapacidad.Visible = true;
                    btn_Actualizar.Enabled = true;
                    btn_Ingresar.Enabled = false;
                }
                catch (System.Exception excep)
                {
                    MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(lbl_Nombre.Text) || dtp_FecEmision.Value == null || dtp_FecInicio.Value == null || dtp_FecFinal.Value == null || cbxTipoIncapacidad.SelectedItem == null || string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    incapacidad.MModificarIncapacidad(Codigo, Convert.ToInt32(cbxTipoIncapacidad.SelectedValue), txtCedula.Text, dtp_FecEmision.Value, dtp_FecInicio.Value, dtp_FecFinal.Value, txtDescripcion.Text);
                    bitacora.MInsertarBitacora("Modificar", "Modificacion Incapacidad", NomUser, DateTime.Today);
                    CargarGridIncapacidades();
                    Limpiar();
                    lbl_Enca_Dias.Visible = false;
                    lbl_Dias_Incapacidad.Visible = false;
                    MessageBox.Show("Incapacidad editada", "Actualización Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al editar Incapacidad. Verifique nuevamente.", "Actualización Incapacidad", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            lbl_Enca_Dias.Visible = false;
            lbl_Dias_Incapacidad.Visible = false;
            this.Limpiar();
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=27 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }

    }
}
