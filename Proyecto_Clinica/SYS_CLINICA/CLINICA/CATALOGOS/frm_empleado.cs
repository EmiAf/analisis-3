﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frm_empleado : Form
    {
        CONEXION.Empleado empleado = new CONEXION.Empleado();
        CONEXION.Rol rol = new CONEXION.Rol();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        private DataSet dsGrid;
        private DataSet dsRol;
        public string NomUser;

        public frm_empleado()
        {
            InitializeComponent();
        }
        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                 if(string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtPrimerApe.Text) || string.IsNullOrEmpty(txtSegundoApe.Text) ||string.IsNullOrEmpty(txtTelefono.Text) || string.IsNullOrEmpty(txtCorreo.Text) || string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtContrasena.Text))
                {
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);                 
                }
                 else if (!txtCedula.MaskCompleted)
                 {
                     MessageBox.Show("Complete de manera adecuada el campo Cédula.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                 }
                 else if (!txtTelefono.MaskCompleted)
                 {
                     MessageBox.Show("Complete de manera adecuada el campo de Télefono.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                 }
                else
                {
                    empleado.MInsertarEmpleado(txtCedula.Text, Int32.Parse(cbxRol.SelectedValue.ToString()), txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, txtTelefono.Text, txtCorreo.Text, txtUsuario.Text, txtContrasena.Text);
                    bitacora.MInsertarBitacora("Insertar", "Insercion Empleado ", NomUser, DateTime.Today);
                    CargarGridEmpleado();
                    MessageBox.Show("El empleado se agregó con éxito", "Ingreso Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Limpiar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al registrar empleado. Verifique nuevamente.", "Ingreso Empleado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void frm_empleado_Load(object sender, EventArgs e)
        {
            this.NomUser = Session.UserSesion;
            CargarComboActivo();
            CargarGridEmpleado();
            CargarComboRol();
            lbl_estado.Visible = false;
            cb_activar.Visible = false;
            label9.Visible = false;
            cbxActivo.Visible = false;
            label21.Visible = false;            
            checkBox1.Visible = false;
            txtContrasena.Enabled = true;
        }
        private void CargarComboActivo()
        {
            DataSet dsActivo = rol.MConsultarTodosRolesCombo();
            cbxActivo.DataSource = dsActivo.Tables[0];
            cbxActivo.DisplayMember = "Activo";
            cbxActivo.ValueMember = "Inactivo";
        }
        private void CargarGridEmpleado()
        {
            try
            {
                dsGrid = empleado.MConsultarTodosEmpleados();
                dgvEmpleado.DataSource = dsGrid.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
        private void CargarComboRol()
        {
            dsRol = rol.MConsultarTodosRoles();
            cbxRol.DataSource = dsRol.Tables[0];
            cbxRol.DisplayMember = "Nombre";
            cbxRol.ValueMember = "Id_Rol";
        }
        private void dgvEmpleado_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            if ((e.ColumnIndex == 0 || e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5
                || e.ColumnIndex == 6 || e.ColumnIndex == 7 || e.ColumnIndex == 8 || e.ColumnIndex == 9 ) && e.RowIndex >= 0)
                {
                    try
                    {
                        txtContrasena.ReadOnly = true;
                        txtCedula.Text = dgvEmpleado.CurrentRow.Cells[0].Value.ToString();

                        string rol = dgvEmpleado.CurrentRow.Cells[1].Value.ToString();
                        if (rol.Trim() == "Admin")
                        {
                            cbxRol.SelectedIndex = 0;
                        }
                        else if (rol.Trim() == "Médico")
                        {
                            cbxRol.SelectedIndex = 1;
                        }
                        else if (rol.Trim() == "Secretaria")
                        {
                            cbxRol.SelectedIndex = 2;
                        }
                        else if (rol.Trim() == "Enfermera")
                        {
                            cbxRol.SelectedIndex = 3;
                        }                        
                        txtNombre.Text = dgvEmpleado.CurrentRow.Cells[2].Value.ToString();
                        txtPrimerApe.Text = dgvEmpleado.CurrentRow.Cells[3].Value.ToString();
                        txtSegundoApe.Text = dgvEmpleado.CurrentRow.Cells[4].Value.ToString();
                        txtTelefono.Text = dgvEmpleado.CurrentRow.Cells[5].Value.ToString();
                        txtCorreo.Text = dgvEmpleado.CurrentRow.Cells[6].Value.ToString();
                        txtUsuario.Text = dgvEmpleado.CurrentRow.Cells[7].Value.ToString();                        
                        
                        string currentActivo = dgvEmpleado.CurrentRow.Cells[9].Value.ToString();
                        if (currentActivo.Trim() == "A")//Usuario Activo
                        {
                            cbxActivo.SelectedIndex = 0;
                            lbl_estado.Text = "Activo";
                            lbl_estado.Visible = true;
                            cb_activar.Visible = false;
                            label9.Visible = true;
                            label21.Visible = true;
                            cb_activar.Visible = false;
                            btn_eliminar.Enabled = true;
                        }
                        else if (currentActivo.Trim() == "I")//Usuario Inactivo
                        {
                            cbxActivo.SelectedIndex = 1;
                            lbl_estado.Text = "Inactivo";
                            lbl_estado.Visible = true;
                            cb_activar.Visible = true;
                            label9.Visible = true;
                            label21.Visible = true;
                            btn_eliminar.Enabled = false;
                        }

                        checkBox1.Visible = true;
                        txtContrasena.Text = dgvEmpleado.CurrentRow.Cells[8].Value.ToString();
                        txtCedula.ReadOnly = true;                        
                        btn_Actualizar.Enabled = true;                        
                        btn_Ingresar.Enabled = false;
                    }
                    catch (System.Exception excep)
                    {
                        MessageBox.Show("Error al cargar datos", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            
        }
        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text) || string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtPrimerApe.Text) || string.IsNullOrEmpty(txtSegundoApe.Text) || string.IsNullOrEmpty(txtTelefono.Text) || string.IsNullOrEmpty(txtCorreo.Text) || string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtContrasena.Text) || cbxActivo.SelectedItem == null)
                {                    
                    MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!txtTelefono.MaskCompleted)
                {
                    MessageBox.Show("Complete de manera adecuada el campo de Télefono.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (cb_activar.Checked == true)
                    {
                        empleado.MModificarEmpleado(txtCedula.Text, Int32.Parse(cbxRol.SelectedValue.ToString()), txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, txtTelefono.Text, txtCorreo.Text, txtUsuario.Text, txtContrasena.Text, "A");
                        bitacora.MInsertarBitacora("Modificar", "Modificacion Empleado", NomUser, DateTime.Today);
                        CargarGridEmpleado();
                        MessageBox.Show("Empleado editado con éxito", "Actualizacíon Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Limpiar();
                    }
                    else
                    {
                        empleado.MModificarEmpleadoAlternative(txtCedula.Text, Int32.Parse(cbxRol.SelectedValue.ToString()), txtNombre.Text, txtPrimerApe.Text, txtSegundoApe.Text, txtTelefono.Text, txtCorreo.Text, txtUsuario.Text, txtContrasena.Text);
                        bitacora.MInsertarBitacora("Modificar", "Modificacion Empleado", NomUser, DateTime.Today);
                        CargarGridEmpleado();
                        MessageBox.Show("Empleado editado con éxito", "Actualizacíon Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Limpiar();
                    }                    
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al editar Empleado. Verifique nuevamente.", "Actualizacíon Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCedula.Text))
                {
                    MessageBox.Show("Existen campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {                    
                    empleado.MEliminarEmpleado(txtCedula.Text);
                    bitacora.MInsertarBitacora("Inhabilitar", "Inhabilitar Empleado", NomUser, DateTime.Today);
                    CargarGridEmpleado();
                    MessageBox.Show("Empleado Inhabilitado", "Sistema Clinica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Limpiar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al inhabilitar. Verifique nuevamente.", "Inhabilitar Empleado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        public void inicio()
        {
            btn_Actualizar.Enabled = false;
            btn_eliminar.Enabled = false;
            btn_Ingresar.Enabled = true;
            //txtContrasena.Enabled = true;
            txtContrasena.ReadOnly = false;
        }

        private void Limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            txtPrimerApe.Clear();
            txtSegundoApe.Clear();
            txtTelefono.Clear();
            txtCorreo.Clear();
            txtUsuario.Clear();
            txtContrasena.Clear();
            cbxActivo.SelectedIndex = -1;
            cbxRol.SelectedIndex = -1;
            txtCedula.ReadOnly = false;
            lbl_estado.Visible = false;
            label9.Visible = false;
            label21.Visible = false;
            cb_activar.Visible = false;
            
            cb_activar.Visible = false;
            cb_activar.Checked = false;
            checkBox1.Visible = false;
            txtContrasena.Enabled = true;
            txtContrasena.ReadOnly = false;
            checkBox1.Checked = false;            
            inicio();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtContrasena.ReadOnly = false;
            }
            else
            {
                txtContrasena.ReadOnly = true;
            }
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=6 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }      
    }
}
