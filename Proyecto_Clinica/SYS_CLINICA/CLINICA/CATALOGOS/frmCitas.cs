﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLINICA;
using System.Globalization;
using System.IO;
using System.Diagnostics;

namespace CLINICA
{
    public partial class frmCitas : Form
    {
        CONEXION.ReservaCitas citas = new CONEXION.ReservaCitas();
        CONEXION.Bitacora bitacora = new CONEXION.Bitacora();
        public string NomUser;
        public frmCitas()
        {
            InitializeComponent();
            MostrarCitas();
            this.NomUser = Session.UserSesion;
        }

        public void EsconderConfirmar(bool esconder)
        {
            this.btnConfirmar.Visible = esconder;
        }
        public void limpiar()
        {
            txtNumCita.Clear();
            txtNombre.Clear();
            txtIdentif.Clear();
            txtTelef.Clear();
            comboH.SelectedIndex = -1;
            comboM.SelectedIndex = -1;
            DtFecha.Text = String.Empty;
        }
        private void MostrarCitas()
        {
            try
            {
                DataSet ds = citas.MostrarCitas();
                dgvCitas.DataSource = ds.Tables[0];
            }
            catch
            {
                MessageBox.Show("Error cargar los datos.", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void lkLblMenu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombre.Text) || !txtIdentif.MaskFull || !txtTelef.MaskFull)
            {
                MessageBox.Show("Existen campos vacíos. Verifique nuevamente.", "Validación de datos.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DateTime fecha = Convert.ToDateTime(DtFecha.Text);
                String cedula = txtIdentif.Text, nombre = txtNombre.Text, telefono = txtTelef.Text;
                String hora = comboH.SelectedItem.ToString(), minutos = comboM.SelectedItem.ToString(), horaCita = hora + ":" + minutos;
                int id = Convert.ToInt32(txtNumCita.Text);
                citas.ModificarCita(id,cedula,nombre,telefono,fecha,horaCita);
                bitacora.MInsertarBitacora("Modificar", "Modificacion Cita", NomUser, DateTime.Today);
                MessageBox.Show("La cita se modificó con éxito", "Ingreso Citas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MostrarCitas();
                limpiar();
            }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
            this.EsconderConfirmar(false);
            this.MostrarCitas();
        }
        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombre.Text) || !txtIdentif.MaskFull || !txtTelef.MaskFull || string.IsNullOrEmpty(comboH.Text) || string.IsNullOrEmpty(comboM.Text))
            {
                MessageBox.Show("Existen campos vacios, por favor verificar.", "Validacion de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                String fecha_actual = DateTime.Now.ToShortDateString();
                String hora_actual = DateTime.Now.ToString("HH:mm", CultureInfo.CurrentCulture);
                String hora = comboH.SelectedItem.ToString(), minutos = comboM.SelectedItem.ToString(), horaCita = hora + ":" + minutos;
                DateTime fecha = Convert.ToDateTime(DtFecha.Text);
                DateTime actual = Convert.ToDateTime(fecha_actual);
                
                if (fecha < actual)
                {
                    MessageBox.Show("Fecha de cita incorrecta", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (fecha == actual)
                {
                    int i = DateTime.Compare(Convert.ToDateTime(hora_actual), Convert.ToDateTime(horaCita));
                    if (i < 0)
                    {
                        String cedula = txtIdentif.Text, nombre = txtNombre.Text, telefono = txtTelef.Text;
                        String respuesta = citas.ValidarCita(fecha, horaCita);
                        if (respuesta.Equals("LIBRE"))
                        {
                            citas.ReservarCita(cedula, nombre, telefono, fecha, horaCita);
                            bitacora.MInsertarBitacora("Insertar", "Insercion Cita", NomUser, DateTime.Today);
                            MessageBox.Show("La cita se guardó con exito", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DialogResult dialogResult = MessageBox.Show("Desea Confirmar la Cita?", "Consulta.", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)
                            {
                               DataSet ds;
                               ds = citas.BuscarIdReservacion(txtIdentif.Text);
                               txtNumCita.Text = ds.Tables[0].Rows[0][0].ToString();

                               this.EsconderConfirmar(true);                                  
                               MostrarCitas();
                            }
                            else
                            {
                                this.EsconderConfirmar(false);
                                MostrarCitas();
                                limpiar();
                            }
                        }
                        else if (respuesta.Equals("OCUPADO"))
                        {
                            MessageBox.Show("Ya hay una cita reservada para esa fecha y a esa hora" + "\r" + "\n" + "Por Favor seleccione otra hora o fecha.", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (i == 0)
                    {
                        String cedula = txtIdentif.Text, nombre = txtNombre.Text, telefono = txtTelef.Text;
                        String respuesta = citas.ValidarCita(fecha, horaCita);
                        if (respuesta.Equals("LIBRE"))
                        {
                            citas.ReservarCita(cedula, nombre, telefono, fecha, horaCita);
                            bitacora.MInsertarBitacora("Insertar", "Insercion Cita", NomUser, DateTime.Today);
                            MessageBox.Show("La cita se guardó con exito", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DialogResult dialogResult = MessageBox.Show("Desea Confirmar la Cita?", "Consulta.", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)
                            {
                               DataSet ds;
                               ds = citas.BuscarIdReservacion(txtIdentif.Text);
                               txtNumCita.Text = ds.Tables[0].Rows[0][0].ToString();

                               this.EsconderConfirmar(true);                                  
                               MostrarCitas();
                            }
                            else
                            {
                                this.EsconderConfirmar(false);
                                MostrarCitas();
                                limpiar();
                            }
                        }
                        else if (respuesta.Equals("OCUPADO"))
                        {
                            MessageBox.Show("Ya hay una cita reservada para esa fecha y a esa hora" + "\r" + "\n" + "Por Favor seleccione otra hora o fecha.", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                        MessageBox.Show("Hora de cita incorrecta", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    String cedula = txtIdentif.Text, nombre = txtNombre.Text, telefono = txtTelef.Text;
                    String respuesta = citas.ValidarCita(fecha, horaCita);
                    if (respuesta.Equals("LIBRE"))
                    {
                        citas.ReservarCita(cedula, nombre, telefono, fecha, horaCita);
                        bitacora.MInsertarBitacora("Insertar", "Insercion Cita", NomUser, DateTime.Today);
                        MessageBox.Show("La cita se guardó con exito", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult dialogResult = MessageBox.Show("Desea Confirmar la Cita?", "Consulta.", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            DataSet ds;
                            ds = citas.BuscarIdReservacion(txtIdentif.Text);
                            txtNumCita.Text = ds.Tables[0].Rows[0][0].ToString();

                            this.EsconderConfirmar(true);
                            MostrarCitas();
                        }
                        else
                        {
                            this.EsconderConfirmar(false);
                            MostrarCitas();
                            limpiar();
                        }
                    }
                    else if (respuesta.Equals("OCUPADO"))
                    {
                        MessageBox.Show("Ya hay una cita reservada para esa fecha y a esa hora" + "\r" + "\n" + "Por Favor seleccione otra hora o fecha.", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        private void label8_Click(object sender, EventArgs e)
        {
            MostrarCitas();
        }
        private void btnBuscarCita_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds1 = citas.ConsultaCitaPorCedula(txtIdentif.Text);
                dgvCitas.DataSource = ds1.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Paciente no registrado.", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            frm_paciente paciente = new frm_paciente();
            citas.ProcesarCita(txtNumCita.Text);
            paciente.txtCedula.Text = this.txtIdentif.Text;
            this.Close();
            paciente.Show();
        }
        private void dgvCitas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtNumCita.Text = dgvCitas.CurrentRow.Cells[0].Value.ToString();
                txtIdentif.Text = dgvCitas.CurrentRow.Cells[1].Value.ToString();
                txtNombre.Text = dgvCitas.CurrentRow.Cells[2].Value.ToString();
                txtTelef.Text = dgvCitas.CurrentRow.Cells[3].Value.ToString();
                DtFecha.Text = dgvCitas.CurrentRow.Cells[4].Value.ToString();
                String hora = dgvCitas.CurrentRow.Cells[5].Value.ToString();
                String[] separar = hora.Split(':');
                comboH.Text = separar[0];
                comboM.Text = separar[1];
                this.EsconderConfirmar(true);
            }
            catch (System.Exception excep)
            {
                MessageBox.Show("Error al cargar los datos.", "Cargar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe", "/A page=34 C:\\Proyecto_Versionado_Analisis\\ProyectoAnalisis03\\Ayuda\\Manual.pdf");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process.Start(startInfo);
        }
    }
}
